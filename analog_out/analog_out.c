//analog output driver
//make sure the board jumpers correspond to the type of DAC output funtions used
// ex. 5VU, 10Vu, 5VB, 10VB
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <signal.h>
#include <utils.h>
#include "analog_out.h"

static unsigned int base = AIO_BASE_ADDR;

volatile union {
   uint8_t value;
   struct {
     unsigned DAC_REF_EN :1;
     unsigned :7;
   } bits;
} AIO_DAC_Ref_En;

volatile union {
   uint16_t value;
   struct {
     unsigned D :12;
     unsigned :4;
   } bits;
   struct {
     unsigned LSB :8;
     unsigned MSB :8;
   } bytes;
} AIO_DAC_Out;

//inputs: DAC channel (1-4), value to output (raw, 12-bit)
//output: none
void outputDAC_raw(uint8_t chan, uint16_t out_val){
   
   AIO_DAC_Out.bits.D = out_val; //DAC output value
  
   switch(chan){
     case 1: 
       outwv(AIO_DAC_Out.value,DAC_CH1(base));//send value to DAC ch1
       break;
     case 2: 
       outwv(AIO_DAC_Out.value,DAC_CH2(base));//send value to DAC ch2
       break;
     case 3: 
       outwv(AIO_DAC_Out.value,DAC_CH3(base));//send value to DAC ch3
       break;
     case 4: 
       outwv(AIO_DAC_Out.value,DAC_CH4(base));//send value to DAC ch4
       break;
     default: break;
   }
   return;
}

void outputDAC_5VU(uint8_t chan, float voltage){
  uint16_t value_raw;
  value_raw = (uint16_t)(((voltage-DAC_5VU_OFFSET)*DAC_PRECISION)/DAC_VREF/DAC_5VU_GAIN);
  outputDAC_raw(chan, value_raw);
  return;
}

void outputDAC_10VU(uint8_t chan, float voltage){
  uint16_t value_raw;
  value_raw = (uint16_t)(((voltage-DAC_10VU_OFFSET)*DAC_PRECISION)/DAC_VREF/DAC_10VU_GAIN);
  outputDAC_raw(chan, value_raw);
  return;
}

void outputDAC_5VB(uint8_t chan, float voltage){
  uint16_t value_raw;
  value_raw = (uint16_t)(((voltage-DAC_5VB_OFFSET)*DAC_PRECISION)/DAC_VREF/DAC_5VB_GAIN);
  outputDAC_raw(chan, value_raw);
  return;
}

void outputDAC_10VB(uint8_t chan, float voltage){
  uint16_t value_raw;
  value_raw = (uint16_t)(((voltage-DAC_10VB_OFFSET)*DAC_PRECISION)/DAC_VREF/DAC_10VB_GAIN);
  outputDAC_raw(chan, value_raw);
  return;
}

//call this first
void AnalogOut_Init(void){
#if SHOW_TRANSACTIONS
  printf("AIO: Using base address: 0x%X\n", base);
#endif
  //IOPermission(base);
}

//set all DAC outputs to zero
void AnalogOut_Zero(void){
  outputDAC_raw(1, DAC1_ZERO); //zero chan 1
  outputDAC_raw(2, DAC2_ZERO); //zero chan 2
  outputDAC_raw(3, DAC3_ZERO); //zero chan 3
  outputDAC_raw(4, DAC4_ZERO); //zero chan 4
}

//enable DAC outputs
void AnalogOut_Enable(void){
  AIO_DAC_Ref_En.value = 0;
  AIO_DAC_Ref_En.bits.DAC_REF_EN = 1; //enable DAC reference and all other DAC outputs (careful here, might get random DAC outputs, set DAC outputs to zero first)

  outbv(AIO_DAC_Ref_En.value,DAC_REF_EN(base));//enable DAC ref and all channels
}

//disables analog outputs, but saves output value
//AnalogOut_Enable restores previous output value
void AnalogOut_Disable(void){
  AIO_DAC_Ref_En.value = 0;
  AIO_DAC_Ref_En.bits.DAC_REF_EN = 0; //enable DAC reference and all other DAC outputs (careful here, might get random DAC outputs, set DAC outputs to zero first)

  outbv(AIO_DAC_Ref_En.value,DAC_REF_EN(base));//enable DAC ref and all channels
}


