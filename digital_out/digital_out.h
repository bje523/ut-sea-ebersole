#ifndef _DIGITAL_OUT_H
#define _DIGITAL_OUT_H

#ifndef AIO_BASE_ADDR
#define AIO_BASE_ADDR 0x300
#endif

#define DIO_A(addr)       ( (addr) + 0x10 )
#define DIO_B(addr)       ( (addr) + 0x11 )
#define DIO_C(addr)       ( (addr) + 0x12 )
#define DIO_CMD(addr)     ( (addr) + 0x13 )
#define DIO_BUFFCTL(addr) ( (addr) + 0x14 )

#ifndef AIO_ADDR_SZ
#define AIO_ADDR_SZ 0x19 //number of IO addresses needed 
#endif

//call this first
void DigitalOut_Init(void);

//assign value[7:0] to port A[7:0]
void setPortA(uint8_t value);

//assign value[7:0] to port B[7:0]
void setPortB(uint8_t value);

//assign value[7:0] to port C[7:0]
void setPortC(uint8_t value);

//assign value[7:0] to port A[7:0]
void clrPortA(uint8_t value);

//assign value[7:0] to port B[7:0]
void clrPortB(uint8_t value);

//assign value[7:0] to port C[7:0]
void clrPortC(uint8_t value);

//xor value[7:0] with port A[7:0]
void togglePortA(uint8_t value);

//xor value[7:0] with port B[7:0]
void togglePortB(uint8_t value);

//xor value[7:0] with port C[7:0]
void togglePortC(uint8_t value);

#endif //_DIGITAL_OUT_H
