//digital output test code
//toggles all ports high and low
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <signal.h>
#include <utils.h>
#include "digital_out.h"

static int End=0;

static void endme(int dummy) { End=1; }

int main(void) {

   signal(SIGINT, endme); //ctrl-c out gracefully
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   DigitalOut_Init();

   while(!End){
     /*
     printf("setting all ports low\n");
     clrPortA(0xFF);
     clrPortB(0xFF);
     clrPortC(0xFF);
     usleep(1000000);
     printf("setting all ports high\n");
     setPortA(0xFF);
     setPortB(0xFF);
     setPortC(0xFF);
     usleep(1000000);
     */

     printf("toggling all ports\n");
     togglePortA(0xFF);
     togglePortB(0xFF);
     togglePortC(0xFF);
     usleep(1000000);
   }

   return 0;
}

