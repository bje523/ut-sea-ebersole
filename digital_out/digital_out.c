//digital output driver
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <signal.h>
#include <utils.h>
#include "digital_out.h"

static unsigned int base = AIO_BASE_ADDR;

static uint8_t PortA = 0x0;
static uint8_t PortB = 0x0;
static uint8_t PortC = 0x0;

volatile union {
   uint8_t value;
   struct {
     unsigned C_DIR :1;
     unsigned B_DIR :1;
     unsigned B_MODE :1;
     unsigned CU_DIR :1;
     unsigned A_DIR :1;
     unsigned A_MODE :2;
     unsigned MODE_SET_FLG :1;
   } bits;
} AIO_DIO_CMD;

volatile union {
   uint8_t value;
   struct {
     unsigned TST :1;
     unsigned :7;
   } bits;
} AIO_DIO_BUFF_CTL;

//call this first
void DigitalOut_Init(void){
#if SHOW_TRANSACTIONS
  printf("AIO: Using base address: 0x%X\n", base);
#endif
  //IOPermission(base, AIO_ADDR_SZ);

  AIO_DIO_BUFF_CTL.value = 0;
  AIO_DIO_BUFF_CTL.bits.TST = 1;

  outbv(AIO_DIO_BUFF_CTL.value,DIO_BUFFCTL(base)); //set tristate mode

  AIO_DIO_CMD.value = 0;
  AIO_DIO_CMD.bits.C_DIR = 0; //port C is output
  AIO_DIO_CMD.bits.B_DIR = 0; //port B is output
  AIO_DIO_CMD.bits.B_MODE = 0;
  AIO_DIO_CMD.bits.CU_DIR = 0;
  AIO_DIO_CMD.bits.A_DIR = 0; //port A is output
  AIO_DIO_CMD.bits.A_MODE = 0;
  AIO_DIO_CMD.bits.MODE_SET_FLG = 1;

  outbv(AIO_DIO_CMD.value,DIO_CMD(base));

  AIO_DIO_CMD.bits.MODE_SET_FLG = 0;

  outbv(AIO_DIO_CMD.value,DIO_CMD(base));
}

//assign value[7:0] to port A[7:0]
void setPortA(uint8_t value){
  PortA |= value;
  outbv(PortA,DIO_A(base));
}

//assign value[7:0] to port B[7:0]
void setPortB(uint8_t value){
  PortB |= value;
  outbv(PortB,DIO_B(base));
}

//assign value[7:0] to port C[7:0]
void setPortC(uint8_t value){
  PortC |= value;
  outbv(PortC,DIO_C(base));
}

//assign value[7:0] to port A[7:0]
void clrPortA(uint8_t value){
  PortA &= ~value;
  outbv(PortA,DIO_A(base));
}

//assign value[7:0] to port B[7:0]
void clrPortB(uint8_t value){
  PortB &= ~value;
  outbv(PortB,DIO_B(base));
}

//assign value[7:0] to port C[7:0]
void clrPortC(uint8_t value){
  PortC &= ~value;
  outbv(PortC,DIO_C(base));
}

//xor value[7:0] with port A[7:0]
void togglePortA(uint8_t value){
  PortA ^= value;
  outbv(PortA,DIO_A(base));
}

//xor value[7:0] with port B[7:0]
void togglePortB(uint8_t value){
  PortA ^= value;
  outbv(PortB,DIO_B(base));
}

//xor value[7:0] with port C[7:0]
void togglePortC(uint8_t value){
  PortA ^= value;
  outbv(PortC,DIO_C(base));
}

