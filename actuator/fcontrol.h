//force feedback control with dynamic compensation

#ifndef _FCONTROL_H
#define _FCONTROL_H

#define FCONTROL_FREQ_HZ 1000 //dynamic compensation filter coefficients have to be recalculated if this is changed
#define FCONTROL_PERIOD_SEC (1/(float)FCONTROL_FREQ_HZ)

#define FCONTROL_DYNFF_EN 0
#define FCONTROL_DOB_EN 1
#define FCONTROL_PID_EN 1
#define FCONTROL_JCOMP_EN 0

#define P_GAIN_DEFAULT 0.002
#define I_GAIN_DEFAULT 0.0
#define D_GAIN_DEFAULT 0.0

#define FCONTROL_DOB_FC 50 //hz
#define FCONTROL_FC 20 //hz

#define FCONTROL_DOB_WC 2*PI*FCONTROL_DOB_FC
#define FCONTROL_WC 2*PI*FCONTROL_DOB_FC

//default filter coeff values
//Tdob filter
//fc = 75hz
#define TDOB_IN1 573.5342537
#define TDOB_IN2 -1126.947226 
#define TDOB_IN3 561.3660864
#define TDOB_OUT1 1.3602091
#define TDOB_OUT2 -0.5201146

//Q_T filter
//fc = 50hz
#define QT_IN1 0.01978958
#define QT_IN2 0.03957916
#define QT_IN3 0.01978958
#define QT_OUT1 1.564504
#define QT_OUT2 -0.6436623

//FF filter
//fc = 50hz
#define FF_IN1 40.843041329
#define FF_IN2 -39.061123149
#define FF_IN3 -40.27914008
#define FF_IN4 39.62502439
#define FF_OUT1 2.3791313
#define FF_OUT2 -1.936633
#define FF_OUT3 0.5348267

//set pid gains
void fcontrol_init(float p, float i, float d);

//performs pid and feed forward control on motor current to servo actuator to desired force
//returns the desired motor torque
float servoActuatorForce(float force_des_n, float spring_force_n, float motor_angle_rad);

#endif //_FCONTROL_H


