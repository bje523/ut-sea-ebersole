#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include "actuator_utils.h"
#include "fcontrol.h"
#include <digital_out.h>
#include <rtai_shm.h>
#include <rtai_fifos.h>

static float Rel_spring_pos_mm_offset=0;
static float Rel_bs_pos_m_offset=0;

//absolute values
static float Init_spring_pos_mm;
static float Init_arm_ang_rad;
static float Init_bs_pos_m;

static float Motor_torque_des=0.0;
static unsigned char FirstRun=1;

//digital filter vars
static float Vm_in_prev[2] = {0,0};
static float Vm_out_prev[2] = {0,0};
static float Vm_in1=VM_IN1, Vm_in2=VM_IN2, Vm_in3=VM_IN3, Vm_out1=VM_OUT1, Vm_out2=VM_OUT2;

static float Va_in_prev[2] = {0,0};
static float Va_out_prev[2] = {0,0};
static float Va_in1=VA_IN1, Va_in2=VA_IN2, Va_in3=VA_IN3, Va_out1=VA_OUT1, Va_out2=VA_OUT2;

static float A_in_prev[2] = {0,0};
static float A_out_prev[2] = {0,0};
static float A_in1=A_IN1, A_in2=A_IN2, A_in3=A_IN3, A_out1=A_OUT1, A_out2=A_OUT2;

static float Fmeas_in_prev[2] = {0,0};
static float Fmeas_out_prev[2] = {0,0};
static float Fmeas_in1=FMEAS_IN1, Fmeas_in2=FMEAS_IN2, Fmeas_in3=FMEAS_IN3, Fmeas_out1=FMEAS_OUT1, Fmeas_out2=FMEAS_OUT2;

#if ENABLE_SIMPLE_DERIVATIVE_MEASUREMENT
static float Motor_ang_prev=0.0, Motor_vel_prev=0.0;
#endif

//generated using gen_vfilter.m
static void calculateVfilterCoeffs(float w_c, float t_s, float *in1, float*in2, float *in3, float *out1, float *out2){
  float den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;
  *in1 = 5000*t_s*w_c*w_c / den;
  *in2 = 0;
  *in3 = -5000*t_s*w_c*w_c / den;
  *out1 = -(5000*t_s*t_s*w_c*w_c - 20000) / den;
  *out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;
  //printf("v\nf_c:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nout1:%f\nout2:%f\n", w_c/2/PI, t_s, *in1, *in2, *in3, *out1, *out2);
}

//generated using gen_afilter.m
static void calculateAfilterCoeffs(float w_c, float t_s, float *in1, float*in2, float *in3, float *out1, float *out2){
  float den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;
  *in1 = 10000*w_c*w_c / den;
  *in2 = -20000*w_c*w_c / den;
  *in3 = 10000*w_c*w_c / den;
  *out1 = -(5000*t_s*t_s*w_c*w_c - 20000) / den;
  *out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;
  //printf("a\nf_c:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nout1:%f\nout2:%f\n", w_c/2/PI, t_s, *in1, *in2, *in3, *out1, *out2);
}

//generated using gen_fmeasfilter.m
static void calculateFmeasfilterCoeffs(float w_c, float t_s, float *in1, float*in2, float *in3, float *out1, float *out2){
  float den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;
  *in1 = w_c*w_c*(2500*SPRING_CONST_NpM*t_s*t_s + 5000*B_FIT*t_s + 10000*M_FIT) / den;
  *in2 = -w_c*w_c*(20000*M_FIT - 5000*SPRING_CONST_NpM*t_s*t_s) / den;
  *in3 = w_c*w_c*(2500*SPRING_CONST_NpM*t_s*t_s - 5000*B_FIT*t_s + 10000*M_FIT) / den;
  *out1 = -(5000*t_s*t_s*w_c*w_c - 20000) / den;
  *out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;
  //printf("fmeas\nf_c:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nout1:%f\nout2:%f\n", w_c/2/PI, t_s, *in1, *in2, *in3, *out1, *out2);
}

static float Vm_filt(float vm_in){
  float vm_out = Vm_in1*vm_in + Vm_in2*Vm_in_prev[0] + Vm_in3*Vm_in_prev[1] + //input component
                  Vm_out1*Vm_out_prev[0] + Vm_out2*Vm_out_prev[1]; //output component

  Vm_in_prev[1] = Vm_in_prev[0];
  Vm_in_prev[0] = vm_in;
  Vm_out_prev[1] = Vm_out_prev[0];
  Vm_out_prev[0] = vm_out;
  return vm_out;                
}

static float Va_filt(float va_in){
  float va_out = Va_in1*va_in + Va_in2*Va_in_prev[0] + Va_in3*Va_in_prev[1] + //input component
                  Va_out1*Va_out_prev[0] + Va_out2*Va_out_prev[1]; //output component

  Va_in_prev[1] = Va_in_prev[0];
  Va_in_prev[0] = va_in;
  Va_out_prev[1] = Va_out_prev[0];
  Va_out_prev[0] = va_out;
  return va_out;                
}

static float A_filt(float a_in){
  float a_out = A_IN1*a_in + A_IN2*A_in_prev[0] + A_IN3*A_in_prev[1] + //input component
                  A_OUT1*A_out_prev[0] + A_OUT2*A_out_prev[1]; //output component

  A_in_prev[1] = A_in_prev[0];
  A_in_prev[0] = a_in;
  A_out_prev[1] = A_out_prev[0];
  A_out_prev[0] = a_out;
  return a_out;                
}

static float Fmeas_filt(float fmeas_in){
  float fmeas_out = Fmeas_in1*fmeas_in + Fmeas_in2*Fmeas_in_prev[0] + Fmeas_in3*Fmeas_in_prev[1] + //input component
                  Fmeas_out1*Fmeas_out_prev[0] + Fmeas_out2*Fmeas_out_prev[1]; //output component
  Fmeas_in_prev[1] = Fmeas_in_prev[0];
  Fmeas_in_prev[0] = fmeas_in;
  Fmeas_out_prev[1] = Fmeas_out_prev[0];
  Fmeas_out_prev[0] = fmeas_out;
  return fmeas_out;                
}

//sample absolute spring position (analog)
//value is in mm
int sampleSpringPos(float *springPos){
  float adc_voltage;
  if( sampleADC_5VU(A_SPRING_DISP_CHAN, &adc_voltage) == ERR_NONE ) {
    *springPos = voltage2springPos_mm(adc_voltage);
    return ERR_NONE;
  }
  else {
    printf("Uh oh..sample error\n");
    return ERR;
  }
}

//sample absolute arm angle (analog)
//value is in radians
int sampleArmAngle(float *armAngle){
  float adc_voltage;
  if( sampleADC_5VU(A_ARM_ANGLE_CHAN, &adc_voltage) == ERR_NONE ) {
    *armAngle = voltage2armAng_rad(adc_voltage);
    return ERR_NONE;
  }
  else {
    printf("Uh oh..sample error\n");
    return ERR;
  }
}

//read average sensor values (sum and divide)
int getAvgSensorValues(float *avg_spring_pos_mm, float *avg_arm_ang_rad){
  int i;
  double armAngAcc = 0.0;
  double springPosAcc = 0.0;
  float spring_pos, arm_angle;
  int err=0;
  //sample initial (absolute) positions
  for(i=0; i<INIT_NUM_SAMPLES; i++){
    if(sampleSpringPos(&spring_pos) == ERR) { err = 1; }
    if(sampleArmAngle(&arm_angle) == ERR) { err = 1; }
    springPosAcc += spring_pos;
    armAngAcc += arm_angle;
  }
  *avg_spring_pos_mm = springPosAcc / INIT_NUM_SAMPLES;
  *avg_arm_ang_rad = armAngAcc / INIT_NUM_SAMPLES;
  //printf("init: arm ang: %f\tspring pos: %f\n",rad2deg(*avg_arm_ang_rad), *avg_spring_pos_mm);
  return err;
}

//finds initial (absolute) arm angle and initial (absolute) spring deflection
//uses these values to calculate initial actuator length
//return 0 if no error, 1 if sample error
int sensorInit(void){
  int err=0;
  //start by averaging samples of absolute analog sensors
  err = getAvgSensorValues(&Init_spring_pos_mm, &Init_arm_ang_rad);
   
  //next figure out the actuator length
  Init_bs_pos_m = armAngle_rad2ActuatorLength_m(Init_arm_ang_rad) + Init_spring_pos_mm/1000.0; 
  return err;
}

//write local struct to shared memory
void ass_writeShm(unsigned int fd, Actuator_state_str *Ass, Actuator_state_str *localAss){
  rtf_sem_wait(fd);
  memcpy((void*)Ass, (const void*)localAss, sizeof(Actuator_state_str));
  rtf_sem_post(fd);
}

//check if arm is within a safe operating region
//false = we're ok
//true = aaah!
int armPosSafetyCheck(float Arm_ang_rad){
  //printf("ang:%f\tmax:%f\tmin:%f\n",rad2deg(Arm_ang_rad), SOFT_MAX_ARM_ANGLE_DEG, SOFT_MIN_ARM_ANGLE_DEG);
  return (rad2deg(Arm_ang_rad) > SOFT_MAX_ARM_ANGLE_DEG) || (rad2deg(Arm_ang_rad) < SOFT_MIN_ARM_ANGLE_DEG);
}

//check if arm torque is within a safe operating region
//false = we're ok
//true = aaah!
int armTorqueSafetyCheck(float Arm_torque_nm){
  return abs(Arm_torque_nm) >= MAX_TORQUE_NM;
}

//check if ballscrew force is within a safe operating region
//false = we're ok
//true = aaah!
int springForceSafetyCheck(float Arm_force_n){
  return abs(Arm_force_n) >= MAX_FORCE_N;
}

//updates the state variables of the actuator
//safe = 1, check to make sure values are within safe operating levels
//return 0 if safe (or if safe=0), 1 if not
int updateState(Actuator_state_str *localAss, char safe){
  float rel_spring_pos_mm, rel_bs_pos_m;
  float bs_pos_m, actuator_len_m;
  float spring_pos_mm;
  
  rel_spring_pos_mm = sensorAng2springPos_mm(Quad_getAngle_rad(Q_SPRING_ANGLE_CHAN)) - Rel_spring_pos_mm_offset;
  localAss->Motor_angle_rad = Quad_getAngle_rad(Q_MOTOR_ANGLE_CHAN);
  rel_bs_pos_m = motorAng2bsPos_m(localAss->Motor_angle_rad) - Rel_bs_pos_m_offset;
  spring_pos_mm = Init_spring_pos_mm - rel_spring_pos_mm;
  bs_pos_m = Init_bs_pos_m + rel_bs_pos_m;
  actuator_len_m = bs_pos_m - spring_pos_mm/1000.0;

  if(FirstRun){
    Vm_in_prev[0] = localAss->Motor_angle_rad;
    Vm_in_prev[1] = Vm_in_prev[0];
    A_in_prev[0] = localAss->Motor_angle_rad;
    A_in_prev[1] = A_in_prev[0];
    Fmeas_in_prev[0] = spring_pos_mm/1000.0;
    Fmeas_in_prev[1] = Fmeas_in_prev[0];
    Fmeas_out_prev[0] = springPos_mm2springForce_N(spring_pos_mm);
    Fmeas_out_prev[1] = Fmeas_out_prev[0]; 
    calculateVfilterCoeffs(VM_WC, FCONTROL_PERIOD_SEC, &Vm_in1, &Vm_in2, &Vm_in3, &Vm_out1, &Vm_out2);
    calculateVfilterCoeffs(VA_WC, FCONTROL_PERIOD_SEC, &Va_in1, &Va_in2, &Va_in3, &Va_out1, &Va_out2);
    calculateAfilterCoeffs(AM_WC, FCONTROL_PERIOD_SEC, &A_in1, &A_in2, &A_in3, &A_out1, &A_out2);
    calculateFmeasfilterCoeffs(FMEAS_WC, FCONTROL_PERIOD_SEC, &Fmeas_in1, &Fmeas_in2, &Fmeas_in3, &Fmeas_out1, &Fmeas_out2);
  }

  localAss->Spring_force_n = springPos_mm2springForce_N(spring_pos_mm);
  localAss->Actuator_force_n = Fmeas_filt(spring_pos_mm/1000.0);
  localAss->Arm_ang_rad = actuatorLen2ArmAngle(actuator_len_m);
  localAss->Arm_torque_nm = actuatorForce2ArmTorque(localAss->Actuator_force_n, localAss->Arm_ang_rad);
  localAss->Motor_vel_rad = Vm_filt(localAss->Motor_angle_rad); //rad/sec
#if ENABLE_SIMPLE_DERIVATIVE_MEASUREMENT
  localAss->Motor_vel_rad_simple = (localAss->Motor_angle_rad - Motor_ang_prev) / FCONTROL_PERIOD_SEC;
  localAss->Motor_acc_rad_simple = (localAss->Motor_vel_rad_simple - Motor_vel_prev) / FCONTROL_PERIOD_SEC;
  Motor_ang_prev = localAss->Motor_angle_rad;
  Motor_vel_prev = localAss->Motor_vel_rad_simple;
#endif
  localAss->Motor_acc_rad = A_filt(localAss->Motor_angle_rad); //rad/sec/sec
  localAss->Motor_mech_power_w = fabs(-Motor_torque_des * localAss->Motor_vel_rad);

  if(FirstRun){
    Va_in_prev[0] = localAss->Arm_ang_rad;
    Va_in_prev[1] = Va_in_prev[0];
  }

  localAss->Arm_vel_rad = Va_filt(localAss->Arm_ang_rad); //rad/sec
  localAss->Arm_power_w = fabs(localAss->Arm_torque_nm * localAss->Arm_vel_rad);
  localAss->Mech_efficiency = -localAss->Arm_power_w / localAss->Motor_mech_power_w;
  if(localAss->Mech_efficiency > 1.0 || localAss->Mech_efficiency < 0.0) localAss->Mech_efficiency = -2.0; //check validity of data
  localAss->Motor_torque_des_nm = Motor_torque_des;
  localAss->Motor_current = -motorTorque2current(localAss->Motor_torque_des_nm);
  localAss->Motor_voltage = calcMotorVoltage(localAss->Motor_current, radps2rpm(localAss->Motor_vel_rad));
  localAss->Motor_elec_power_w = fabs(localAss->Motor_voltage * localAss->Motor_current);
  localAss->Motor_efficiency = localAss->Motor_mech_power_w / localAss->Motor_elec_power_w;
  if(localAss->Motor_efficiency > 1.0 || localAss->Motor_efficiency < 0.0) localAss->Motor_efficiency = -1.0; //check validity of data
  localAss->Total_efficiency = localAss->Motor_efficiency * localAss->Mech_efficiency;

  localAss->newData = 1;
  FirstRun = 0;
  
  //safety checks
  if(safe){
    if(springForceSafetyCheck(localAss->Spring_force_n)){ 
     printf("Spring force (%.2fN) greater than safe value (%.2fN)\n", localAss->Spring_force_n, (float)MAX_FORCE_N);
     return 1;
    }
    else if(armTorqueSafetyCheck(localAss->Arm_torque_nm)){ 
      printf("Arm torque (%.2fNm) greater than safe value (%.2fNm)\n", localAss->Arm_torque_nm, (float)MAX_TORQUE_NM);
      return 1;
    }
    else if(armPosSafetyCheck(localAss->Arm_ang_rad)){ 
      printf("Arm position (%.2fdeg) out of safe region (%.2f->%.2fdeg)\n", rad2deg(localAss->Arm_ang_rad), (float)SOFT_MAX_ARM_ANGLE_DEG, (float)SOFT_MIN_ARM_ANGLE_DEG);
      return 1;
    }
    else { return 0; }
  }
  else return 0;

}

//scale and output to motor and mirror channel if enabled
void setMotorTorque(float torque_des){
  float motor_torque_adj;
  Motor_torque_des = torque_des;
  motor_torque_adj = -Motor_torque_des - TORQUE_OFFSET_ADJUST;
  outputDAC_10VB(MOTOR_CHAN, currentOut2dacVoltage(motorTorque2current(motor_torque_adj)));
#if EN_MIRROR_OUTPUT
  outputDAC_10VB(MIRROR_CHAN, currentOut2dacVoltage(motorTorque2current(motor_torque_adj)));
#endif
  if(fabs(Motor_torque_des) > MAX_CONTINUOUS_MOTOR_TORQUE){
    setPortA(MOTOR_THERMAL_CHAN);
  }
  else{
    clrPortA(MOTOR_THERMAL_CHAN);
  }
}

//if you reset the quad counters in hardware, the incremental spring encoder will develop an offset upon movement
//use this function instead and subtract the measured offset
void quadGetOffset(){
  Rel_spring_pos_mm_offset = sensorAng2springPos_mm(Quad_getAngle_rad(Q_SPRING_ANGLE_CHAN));
  Rel_bs_pos_m_offset = motorAng2bsPos_m(Quad_getAngle_rad(Q_MOTOR_ANGLE_CHAN));
}
