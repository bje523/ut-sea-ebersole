#ifndef _ACTUATOR_PARAMS_H
#define _ACTUATOR_PARAMS_H

//frequently tuned params
#define SPRING_SENSOR_ZERO_OFFSET 4.348  //adjust this if there is force/torque offset, increase this value if there is an upwards torque for gravity compensation, decrease if there is a downward torque
#define ARM_V_PI_RAD 1.595 //sensor voltage for an arm angle of pi radians (change if abs. angle measurement is off)

#define ADDED_MASS_LBS 0.0 //0.0 2.475 5.314 9.793 19.586
#define ADDED_MASS lbs2kg(ADDED_MASS_LBS) //kg
#define ADDED_MASS_DIST .190 //m from arm pivot along arm axis
  // for 0.0                pdobfc = 35 maxforce = 500  maxtorque = 8  chirp_a=15 chirp_f=3 spline_a=30 spline_f=2
  // for 2.475  dist = .190 pdobfc = 13 maxforce = 800  maxtorque = 17 chirp_a=15 chirp_f=2 spline_a=30 spline_f=1.5
  // for 5.314  dist = .190 pdobfc = 13 maxforce = 1200 maxtorque = 30 a=?  f=? spline_a=30 spline_f=1.25
  // for 9.793  dist = .190 pdobfc = 9  maxforce = 1200 maxtorque = 35 a=?  f=? spline_a=30 spline_f=1.0
  // for 19.586 dist = .197 pdobfc = 5  maxforce = 1500 maxtorque = 45 a=10 f=1.2

#define ENABLE_SIMPLE_DERIVATIVE_MEASUREMENT 0

//position limits
#define HARD_MAX_ARM_ANGLE_DEG 170.0
#define HARD_MIN_ARM_ANGLE_DEG 35.0
#define ARM_ANGLE_SAFETY_ZONE_DEG 20.0
#define SOFT_MAX_ARM_ANGLE_DEG (HARD_MAX_ARM_ANGLE_DEG-ARM_ANGLE_SAFETY_ZONE_DEG)
#define SOFT_MIN_ARM_ANGLE_DEG (HARD_MIN_ARM_ANGLE_DEG+ARM_ANGLE_SAFETY_ZONE_DEG)

//force limits
#define MAX_TORQUE_NM 8
#define MAX_FORCE_N 100 

//analog input channels
#define A_SPRING_DISP_CHAN 2
#define A_ARM_ANGLE_CHAN 3

//quadrature input channels
#define Q_MOTOR_ANGLE_CHAN 0
#define Q_SPRING_ANGLE_CHAN 1

//analog output channels
#define MOTOR_CHAN 1
#define MIRROR_CHAN 2

//digital output channels
#define HEARTBEAT_CHAN 1 //port A
#define MOTOR_THERMAL_CHAN 2 //port A

//spring displacement params
#define SPRING_PULLEY1_DIAM_MM 7.239 //absolute sensor
#define SPRING_THROW_MM (PI*SPRING_PULLEY1_DIAM_MM)
#define SUPPLY_V_SPRING 3.9337 //effective supply voltage (tuning param)
#define SPRING_PULLEY2_DIAM_MM 7.239 //incremental sensor
#define SPRING_CONST_NpMM 277.78
#define SPRING_CONST_NpM (SPRING_CONST_NpMM*1000)

//arm position params
#define SUPPLY_V_ARM 4.85 //effective supply voltage (tuning param, affects abs to inc angle scale)
#define ARM_CALIB_ANG PI

//drivetrain params
#define BIG_PULLEY_TEETH 60.0
#define SMALL_PULLEY_TEETH 18.0
#define PULLEY_REDUCTION (BIG_PULLEY_TEETH/SMALL_PULLEY_TEETH)
#define BALLSCREW_LEAD .003 //m
#define DRIVETRAIN_EFF 0.8 //efficiency

//other params
#define INIT_NUM_SAMPLES 1000 //used for averaging initial analog measurement

//mechanical params
#define C_LENGTH_M 0.125//0.15018
#define B_LENGTH_M 0.025
#define SPRUNG_MASS .706 //kg
#define ROTOR_INERTIA 0.00000344900 //kgm^2
#define BALL_NUT_INERTIA 0.000017523 //kgm^2
#define EFFECTIVE_ROTOR_INERTIA (ROTOR_INERTIA + BALL_NUT_INERTIA/PULLEY_REDUCTION/PULLEY_REDUCTION)
#define G 9.80665
#define ARM_MASS 0.54 //kg
#define ARM_X_COM_DIST 0.126 //0.119 0.1436 tuned to match measured inertia
#define ARM_Y_COM_DIST 0.005 //0.0421 tuned to match grav comp
#define ARM_COM_ANG_OFFSET (atan2(ARM_Y_COM_DIST,ARM_X_COM_DIST)) //rad
#define ARM_GEOM_VERT_OFFSET 0.024 //m, distace arm axis is from rotation point
#define COMBINED_ARM_COM_X (ARM_X_COM_DIST*ARM_MASS + ADDED_MASS_DIST*ADDED_MASS)/(ARM_MASS+ADDED_MASS)
#define COMBINED_ARM_COM_Y (ARM_Y_COM_DIST*ARM_MASS + ARM_GEOM_VERT_OFFSET*ADDED_MASS)/(ARM_MASS+ADDED_MASS)
#define COMBINED_ARM_COM_ANG_OFFSET (atan2(COMBINED_ARM_COM_Y,COMBINED_ARM_COM_X))
#define COMBINED_ARM_COM_DIST (sqrt(COMBINED_ARM_COM_Y*COMBINED_ARM_COM_Y + COMBINED_ARM_COM_X*COMBINED_ARM_COM_X))
#define COMBINED_ARM_COM_MASS (ARM_MASS+ADDED_MASS)
#define MOUNT_ANG_OFFSET 0.08 //rad, angle from actuator mount pivot to joint pivot from horizontal
#define COMBINED_ANG_OFFSET (MOUNT_ANG_OFFSET + COMBINED_ARM_COM_ANG_OFFSET) //rad, offset angle of COM from horizontal

//force control
#define M_FIT 19.75 //kg
#define B_FIT 500 //ns/m

//position control
#define ARM_INERTIA (COMBINED_ARM_COM_MASS*COMBINED_ARM_COM_DIST*COMBINED_ARM_COM_DIST) //test this first
#define ARM_FRICTION 0.08 //Nm*s/rad (viscous)

//thermal params
#define MAX_CONTINUOUS_MOTOR_TORQUE 0.128 //Nm

//motor and electrical params
#define DAC_VOLTAGE 10.0
#define ELMO_PEAK_CURRENT 30.0
#define TORQUE_OFFSET_ADJUST 0.004577 //fix offset shift in DAC/AMP
#define TORQUE_CONSTANT .0276 //nm/A
#define SPEED_CONSTANT 345.99 //rpm/v
#define WINDING_RESISTANCE 0.386 //ohms

//conversions
#define rad2deg(rad) ((float)(rad)*180/PI)
#define deg2rad(deg) ((float)(deg)*PI/180)
#define kg2lbs(kg) ((float)(kg)*2.2046226)
#define lbs2kg(lbs) ((float)(lbs)/2.2046226)
#define radps2rpm(radps) (float)(radps)*30/PI
#define voltage2springPos_mm(voltage) ((-((voltage)-SPRING_SENSOR_ZERO_OFFSET)*SPRING_THROW_MM)/SUPPLY_V_SPRING)
#define voltage2armAng_rad(voltage) (((ARM_V_PI_RAD-(float)(voltage))*2*PI)/SUPPLY_V_ARM + ARM_CALIB_ANG)
#define motorAng2bsPos_m(mang) ((float)(mang)*BALLSCREW_LEAD/2/PI/PULLEY_REDUCTION)
#define sensorAng2springPos_mm(sang) (-(float)(sang)*SPRING_PULLEY2_DIAM_MM/2)
#define springPos_mm2springForce_N(pos) ((float)(pos)*SPRING_CONST_NpMM)
#define springForce_N2springPos_mm(force) ((float)(force)/SPRING_CONST_NpMM)
#define armAngle_rad2ActuatorLength_m(angle_rad) sqrt( B_LENGTH_M*B_LENGTH_M + C_LENGTH_M*C_LENGTH_M - 2*B_LENGTH_M*C_LENGTH_M*cos((float)(angle_rad)) ) //finds actuator length given arm angle using law of cosines
#define actuatorForce2ArmTorque(force, angle_rad) ((float)(force)*C_LENGTH_M*B_LENGTH_M*sin((float)(angle_rad))/armAngle_rad2ActuatorLength_m(angle_rad)) //finds arm torge given the actuator force and length
#define actuatorLen2ArmAngle(len) (acos((-((float)(len)*(float)(len)) + B_LENGTH_M*B_LENGTH_M + C_LENGTH_M*C_LENGTH_M)/(2*B_LENGTH_M*C_LENGTH_M) ))
#define currentOut2dacVoltage(current) ((float)(current)*DAC_VOLTAGE/ELMO_PEAK_CURRENT)
#define motorTorque2current(torque)  ((float)(torque)/TORQUE_CONSTANT)
#define calcMotorVoltage(current, rpm) ((float)(current)*WINDING_RESISTANCE + (float)(rpm)/SPEED_CONSTANT)
#define BsForce2motorTorque(force) ((float)(force)*BALLSCREW_LEAD/(PULLEY_REDUCTION*2*PI*DRIVETRAIN_EFF))
#define motorTorque2BsForce(torque) ((float)(torque)*PULLEY_REDUCTION*2*PI*DRIVETRAIN_EFF/BALLSCREW_LEAD)
#define armTorque_nm2BsForce_N(torque, angle_rad) ((float)(torque)*sqrt( B_LENGTH_M*B_LENGTH_M + C_LENGTH_M*C_LENGTH_M - 2*B_LENGTH_M*C_LENGTH_M*cos((float)(angle_rad)) )/(B_LENGTH_M*C_LENGTH_M*sin((float)(angle_rad))))
#define armGravityTorque(angle_rad) (-COMBINED_ARM_COM_MASS*G*COMBINED_ARM_COM_DIST*sin( (float)(angle_rad)+COMBINED_ARM_COM_ANG_OFFSET+MOUNT_ANG_OFFSET) ) //negative from theta+180, negative from torque direction

//shared memory info
#define SHMNAM_ASS "ASS"
#define ASS_FIFO "/dev/rtf0"

typedef struct{
  //timing info
  float SampleTime_s;
  float SamplePeriod_s;

  //actuator state
  float Motor_angle_rad;
  float Motor_vel_rad;
  float Motor_acc_rad;
#if ENABLE_SIMPLE_DERIVATIVE_MEASUREMENT
  float Motor_vel_rad_simple;
  float Motor_acc_rad_simple;
#endif
  float Motor_mech_power_w;
  float Spring_force_n; //doesnt include inertial force, only depends on spring deflection
  float Actuator_force_n;
  float Arm_ang_rad;
  float Arm_vel_rad;
  float Arm_torque_nm;
  float Arm_power_w;
  float Motor_current;
  float Motor_voltage; //calculated from desired motor current and measured motor speed
  float Motor_elec_power_w;
  float Mech_efficiency;
  float Motor_efficiency;
  float Total_efficiency;

  //desired control values
  float Motor_torque_des_nm;
  float Actuator_force_des_n;
  float Arm_ang_des_rad;

  //processing flag
  unsigned char newData;
} Actuator_state_str;

#endif //_ACTUATOR_PARAMS_H







