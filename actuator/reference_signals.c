#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include "reference_signals.h"

static float Arm_ang_init=0.0, Actuator_force_init=0.0;
static unsigned char FirstRun_P=1, FirstRun_F=1;
static unsigned char Direction = 1;
static float t_switch = 0.0;
static float Effective_switching_freq_hz=0.0;
static unsigned char Spline_sequencer=0;
static float Spline_start_time=0.0, Spline_duration=0.0, Spline_finish_time=0.0;
static float Spline_A, Spline_B, Spline_C, Spline_F, Spline_final;

//returns desired force
//max freq for chirp signal set statically
float setDesiredActuatorForce(float elapsedTime, float arm_angle_rad, float actuator_force_n, float switching_amp_nm, float switching_freq_hz, unsigned char mode, unsigned char *end){
  float amplitude, offset, force_des_n;
  amplitude = switching_amp_nm;
  offset = armTorque_nm2BsForce_N( -armGravityTorque(arm_angle_rad), arm_angle_rad ); //grav comp as offset

  if(FirstRun_F){
    Actuator_force_init = actuator_force_n;
    Effective_switching_freq_hz=0.0;
  }

  switch(mode){
    case CHIRP:
      if(elapsedTime > 0.0){
    
        //linear chirp
        //Effective_switching_freq_hz = F_SWEEP_FREQ_HZ_LOW + F_SWEEP_RATE*F_SWEEP_RANGE*t;
        //force_des_n = amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;

        //exponential chirp
        Effective_switching_freq_hz = F_SWEEP_FREQ_HZ_LOW * (pow(F_SWEEP_RATE*F_SWEEP_RANGE,elapsedTime)-1)/log(F_SWEEP_RATE*F_SWEEP_RANGE)/elapsedTime;
        force_des_n = amplitude*sin(elapsedTime * 2 * PI * Effective_switching_freq_hz ) + offset;

        if(Effective_switching_freq_hz > F_SWEEP_FREQ_HZ_HIGH) {
          *end = 1;
        }
      }
      else{
        force_des_n = offset;
      }
      break;
    case CONST:
      force_des_n = amplitude;
      //printf("tg:%f\tang:%f\n", armGravityTorque(arm_angle_rad), rad2deg(arm_angle_rad) );
      break;
    case SQUARE:
      if((int)(elapsedTime*2*switching_freq_hz)%2) { 
        force_des_n = offset + amplitude; 
      }
      else {  
        force_des_n = offset - amplitude; 
      }
      break;
    case SINE:
      force_des_n = amplitude*sin(elapsedTime * 2 * PI * switching_freq_hz) + offset;
      break;
    default:    
      force_des_n = 0.0;
      break;
  }

  if(elapsedTime < F_SETPOINT_BLEND_SLEWRATE){
    force_des_n = elapsedTime/F_SETPOINT_BLEND_SLEWRATE * force_des_n + (1 - elapsedTime/F_SETPOINT_BLEND_SLEWRATE) * Actuator_force_init;
  }

  //printf("force_des_n:%f, time:%f\n",force_des_n, elapsedTime);
  FirstRun_F = 0;
  return force_des_n;
}

//calculates coefficients for the spline of the form 
// angle(t) = a*t^5 + b*t^4 + c*t^3 + angle_init where angle(time_final) = angle_final
static void calcSplineCoeffs(float angle_init, float angle_final, float time_final,  float *a,  float *b,  float *c,  float *f){
  *a = 6*(angle_final - angle_init) / pow(time_final,5);
  *b = 15*(angle_init - angle_final) / pow(time_final,4);
  *c = 10*(angle_final - angle_init) / pow(time_final,3);
  *f = angle_init;
}

//return spline value
// angle(t) = a*t^5 + b*t^4 + c*t^3 + angle_init where angle(time_final) = angle_final
static float getSplineValue(float time, float a, float b, float c, float f){
  //printf("time: %f\n", time);
  return a*pow(time,5) + b*pow(time,4) + c*pow(time,3) + f;
}

//coordinate single spline
//return 1 when spline is complete, 0 otherwise
static int generateSpline(float elapsedTime, float arm_ang_init, float arm_ang_final, float spline_duration, float newSpline, float *arm_ang_des){
  float done=0;

  if(newSpline){
    calcSplineCoeffs(arm_ang_init, arm_ang_final, spline_duration, &Spline_A, &Spline_B, &Spline_C, &Spline_F);
    Spline_start_time = elapsedTime;
    Spline_duration = spline_duration;
    Spline_final = arm_ang_final;
  }

  if(elapsedTime >= Spline_start_time && elapsedTime < Spline_start_time + Spline_duration) { //check to see if we're within the spline time period
    *arm_ang_des = getSplineValue(elapsedTime - Spline_start_time, Spline_A, Spline_B, Spline_C, Spline_F);
  }
  else { //if not, we are already done
    *arm_ang_des = Spline_final;
    done = 1;
  }
  
  return done;
}

//returns desired angle
//max chirp freq set by switching_freq_hz
float setDesiredActuatorAngle(float elapsedTime, float arm_ang_rad, float switching_amp_deg, float switching_freq_hz, float sweep_rate, unsigned char mode, unsigned char *end){
  float amplitude, offset, angle_des_rad, t, sweep_range;
  amplitude = deg2rad(switching_amp_deg);
  offset = deg2rad(P_SETPOINT_MID_POS_DEG);
  sweep_range = switching_freq_hz-P_SWEEP_FREQ_HZ_LOW;

  if(FirstRun_P){
    Arm_ang_init = arm_ang_rad;
    Effective_switching_freq_hz=0.0;
  }

  switch(mode){
    case CHIRP:
      if(elapsedTime > 0.0){   
        if(Direction == 1) {
          t = elapsedTime;
          //linear chirp
          Effective_switching_freq_hz = P_SWEEP_FREQ_HZ_LOW + sweep_rate*sweep_range*t;
          angle_des_rad = amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;

          //exponential chirp
          //Effective_switching_freq_hz = P_SWEEP_FREQ_HZ_LOW * (pow(sweep_rate*sweep_range,t)-1)/log(sweep_rate*sweep_range)/t;
          //force_des_n = amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;
          //printf("%f\t%f\n",Effective_switching_freq_hz, fabs(angle_des_rad - offset) );
          if(Effective_switching_freq_hz >= switching_freq_hz && fabs(angle_des_rad - offset) < deg2rad(0.5) ) {
            Direction = -1;
            t_switch = elapsedTime;
            //printf("switching\n");
          }
        }
        else{
          t = t_switch - (elapsedTime-t_switch);

          if(t > elapsedTime || Effective_switching_freq_hz <= 0.0){
            *end = 1;
            angle_des_rad = offset;
          }
          else{
            //linear chirp
            Effective_switching_freq_hz = P_SWEEP_FREQ_HZ_LOW + sweep_rate*sweep_range*t;
            angle_des_rad = -amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;

            //exponential chirp
            //Effective_switching_freq_hz = P_SWEEP_FREQ_HZ_LOW * (pow(sweep_rate*sweep_range,t)-1)/log(sweep_rate*sweep_range)/t;
            //force_des_n = amplitude*sin(t * 2 * PI * Effective_switching_freq_hz ) + offset;
          }
        }
      }
      else{
        angle_des_rad = offset;
      }
      break;
    case SQUARE:
      if((int)(elapsedTime*2*switching_freq_hz)%2) { 
	    angle_des_rad = offset + amplitude; 
	  }
      else {
	    angle_des_rad = offset - amplitude; 
	  }
      //printf("%d %f %f ", offset, amplitude);
      break;
    case SINE:
      angle_des_rad = amplitude*sin(elapsedTime * 2 * PI * switching_freq_hz) + offset;
      break;
    case SPLINE: //smoothed sawtooth
      //coordinate multi-spline sequence
      switch(Spline_sequencer){
        case 0: //start first spline
          //printf("spline sequence 0\n");
          (int)generateSpline(elapsedTime, offset - amplitude, offset + amplitude, SPLINE_SWITCH_RATIO/switching_freq_hz, 1, &angle_des_rad);
          Spline_sequencer++;
          break;
        case 1: //run until spline finishes
          //printf("spline sequence 1\n");
          if(generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, &angle_des_rad)){
            Spline_sequencer++;
            Spline_finish_time = elapsedTime;
          }
          break;
        case 2: //hold position for a bit before starting next spline
          //printf("spline sequence 2\n");
          if(elapsedTime < Spline_finish_time + SPLINE_HOLD){
            generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, &angle_des_rad);
          }
          else {
            generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, &angle_des_rad);
            Spline_sequencer++;
          }
          break;
        case 3: //start second spline
          //printf("spline sequence 3\n");
          (int)generateSpline(elapsedTime, offset + amplitude, offset - amplitude, (1.0-SPLINE_SWITCH_RATIO)/switching_freq_hz, 1, &angle_des_rad);
          Spline_sequencer++;
          break;
        case 4: //run until spline finishes
          //printf("spline sequence 4\n");
          if(generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, &angle_des_rad)){
            Spline_sequencer++;
            Spline_finish_time = elapsedTime;
          }
          break;
        case 5: //hold position for a bit before starting next spline
          //printf("spline sequence 5\n");
          if(elapsedTime < Spline_finish_time + SPLINE_HOLD){
            generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, &angle_des_rad);
          }
          else {
            generateSpline(elapsedTime, 0.0, 0.0, 0.0, 0, &angle_des_rad);
            Spline_sequencer = 0;
          }
          break;
        default:
          angle_des_rad = arm_ang_rad;
          break;
        }
        break;
    default:
      angle_des_rad = arm_ang_rad;
      break;
  }
  
  if(elapsedTime < P_SETPOINT_BLEND_SLEWRATE){
    angle_des_rad = elapsedTime/P_SETPOINT_BLEND_SLEWRATE * angle_des_rad + (1 - elapsedTime/P_SETPOINT_BLEND_SLEWRATE) * Arm_ang_init;
  }

  FirstRun_P = 0;
  return angle_des_rad;
}
