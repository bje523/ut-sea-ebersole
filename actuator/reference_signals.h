#ifndef _REF_SIG_H
#define _REF_SIG_H

//force params
#define F_SETPOINT_SWITCH_FREQ_HZ_DEFAULT 0.5
#define F_SETPOINT_FORCE_AMPLITUDE_N_DEFAULT 100.0
#define F_SETPOINT_MID_FORCE_N 0.0
#define F_SETPOINT_BLEND_SLEWRATE 2.0 //seconds

//force chirp params
#define F_SWEEP_FREQ_HZ_LOW 0.001
#define F_SWEEP_FREQ_HZ_HIGH 50
#define F_SWEEP_RATE 0.033 //percent change of sweep range per second
#define F_SWEEP_RANGE (F_SWEEP_FREQ_HZ_HIGH-F_SWEEP_FREQ_HZ_LOW)
#define F_SWEEP_SETPOINT_FORCE_AMPLITUDE_N_DEFAULT 20.0
//#define F_SWEEP_SETPOINT_MID_FORCE_N 60.0 //now using grav comp

//position params
#define P_SETPOINT_SWITCH_FREQ_HZ_DEFAULT 0.5
#define P_SETPOINT_POS_AMPLITUDE_DEG_DEFAULT 10.0
#define P_SETPOINT_MID_POS_DEG 90.0//((HARD_MAX_ARM_ANGLE_DEG + HARD_MIN_ARM_ANGLE_DEG)/2)
#define P_SETPOINT_BLEND_SLEWRATE 1.0 //seconds

//position chirp params
#define P_SWEEP_FREQ_HZ_LOW 0.001
#define P_SWEEP_FREQ_HZ_HIGH 1.5
#define P_SWEEP_RATE 0.1 //percent change of sweep range per second

//position spline params
#define SPLINE_SWITCH_RATIO 0.3
#define SPLINE_HOLD 1.0 //wait time between splines

//control modes
enum{ CHIRP, CONST, SQUARE, SINE, SPLINE }; //everything except CONST is offset with gravity compensation in force control mode

//send a variety of reference signals as desired force
//returns desired force
float setDesiredActuatorForce(float elapsedTime, float arm_angle_rad, float actuator_force_n, float switching_amp_nm, float switching_freq_hz, unsigned char mode, unsigned char *end);

//send a variety of reference signals as desired angle
//returns desired angle
float setDesiredActuatorAngle(float elapsedTime, float arm_ang_rad, float switching_amp_deg, float switching_freq_hz, float sweep_rate, unsigned char mode, unsigned char *end);

#endif //_REF_SIG_H
