//sends chirp signal as desired actuator angle and measures angle response
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include "actuator_utils.h"
#include "fcontrol.h"
#include "pcontrol.h"
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include "reference_signals.h"

#define RTAI_TASK_NAME "PRESP" //must be unique

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define TIME_TO_BEGIN 10 //wait this long before starting test

static float Switching_amp_deg = P_SETPOINT_POS_AMPLITUDE_DEG_DEFAULT;
static float MaxFreq_hz = P_SWEEP_FREQ_HZ_HIGH;
static float Sweep_rate = P_SWEEP_RATE;

static unsigned char End=0;
static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -a <amp (nm)>   : setpoint switching amplitude (default: %f)\n", P_SETPOINT_POS_AMPLITUDE_DEG_DEFAULT);
  printf ("  -f <freq (hz)>  : max setpoint switching frequency (default: %f)\n", P_SWEEP_FREQ_HZ_HIGH);
  printf ("  -r <chirp rate> : chirp sweep rate (bigger = shorter duration) (default: %f)\n", P_SWEEP_RATE);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "a:r:f:sh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'a': 
          Switching_amp_deg = atof(optarg);
          break;
      case 'f': 
          MaxFreq_hz = atof(optarg);
          break;
      case 'r': 
          Sweep_rate = atof(optarg);
          break;
      case '?': // help
      case 'h': // help
      case ':':
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

int main(int argc, char **argv) {

   Actuator_state_str localAss; //contains actuator state data
   Actuator_state_str *ass;
   unsigned int rtf_fd;

   RT_TASK *task;
   int period;

   RTIME start_time, elapsedTime_ns, sampleTime_prev_ns=0, currentTime_ns;
   double samplePeriod_s;
   float elapsedTime_s;
   
   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv);

   ass = rtai_malloc(nam2num(SHMNAM_ASS), sizeof(Actuator_state_str));
   rtf_fd = open(ASS_FIFO, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd,1);

   //print message to console
   printf("Controlling position at %d hz fc_dob=%d fc=%d with setpoint amp=%.3f max_freq=%.3f rate=%.3f load mass=%.2flbs\n", PCONTROL_FREQ_HZ, PCONTROL_DOB_FC, PCONTROL_FC, Switching_amp_deg, MaxFreq_hz, Sweep_rate, kg2lbs(ADDED_MASS));
   
   //initialization
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   AnalogIn_Init();
   Quad_init(512, 20000, 500, 500, 500, 500, 500, 500);
   //Quad_resetCounters();
   quadGetOffset(); //issue with resetting spring incremental encoder, do it this way
   AnalogOut_Init(); //request port access
   AnalogOut_Zero(); //sets all outputs to zero value (check jumpers/manual for what voltage this corresponds to)
   AnalogOut_Enable(); //enables analog outputs
#if TOGGLE_DOUT_ON_CYCLE
   DigitalOut_Init();
#endif

   //read absolute positions so we can use relative sensors from here on out
   if(sensorInit()){
     printf("Sample error..ending\n");
     exit(ERR);
   }

   printf("waiting %d seconds to begin..go put the arm at %.0f degrees\n", TIME_TO_BEGIN, P_SETPOINT_MID_POS_DEG);
   sleep(TIME_TO_BEGIN);
   printf("starting...\n");

   //schedule control task
   if (!(task = rt_task_init_schmod(nam2num(RTAI_TASK_NAME), 0, 0, 0, SCHED_FIFO, 0xF))) {
     printf("CANNOT INIT %s TASK\n", RTAI_TASK_NAME);
     exit(1);
   }
   //rt_set_oneshot_mode();
   rt_set_periodic_mode();
   period = start_rt_timer(nano2count(NS_PER_SEC/FCONTROL_FREQ_HZ));
   mlockall(MCL_CURRENT | MCL_FUTURE); //keep program from paging
   rt_make_hard_real_time();
   rt_task_make_periodic(task, rt_get_time() + period, period);

   fflush(stdout);

   //get starting time
   start_time = rt_get_time_ns();
   sampleTime_prev_ns = start_time;

   while(!End){

     //timing
     currentTime_ns = rt_get_time_ns();
     elapsedTime_ns = currentTime_ns - start_time;
     samplePeriod_s = ns2sec(currentTime_ns - sampleTime_prev_ns);
     sampleTime_prev_ns = currentTime_ns;
     elapsedTime_s = ns2sec(elapsedTime_ns);
     //printf("sample period %lf\n", samplePeriod_s);
     if(samplePeriod_s > 0.002) printf(":( %f\t%f\n", samplePeriod_s, elapsedTime_s);

     //printf("elapsedTime:%f\n", elapsedTime_s);
     localAss.SampleTime_s = elapsedTime_s;
     localAss.SamplePeriod_s = samplePeriod_s;

     //read actuator state and put in local struct
     if(updateState(&localAss, 1)){
       End = 1;
       break;
     }

     if(elapsedTime_s <= F_SETPOINT_BLEND_SLEWRATE){ //start by slowly increasing force to grav comp force
       localAss.Actuator_force_des_n = setDesiredActuatorForce(elapsedTime_s, localAss.Arm_ang_rad, localAss.Actuator_force_n, 0.0, 0.0, SINE, &End);
       localAss.Motor_torque_des_nm = servoActuatorForce(localAss.Actuator_force_des_n, localAss.Spring_force_n, localAss.Motor_acc_rad);
     }
     else{ //then transition into position control
       localAss.Arm_ang_des_rad = setDesiredActuatorAngle(elapsedTime_s-F_SETPOINT_BLEND_SLEWRATE, localAss.Arm_ang_rad, Switching_amp_deg, MaxFreq_hz, Sweep_rate, CHIRP, &End);
       servoActuatorAngle(localAss.Arm_ang_des_rad, localAss.Spring_force_n, localAss.Arm_ang_rad, localAss.Motor_acc_rad, &localAss.Actuator_force_des_n, &localAss.Motor_torque_des_nm);
     }

     //write actuator state to shared mem
     ass_writeShm(rtf_fd, ass, &localAss);

#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     rt_task_wait_period();

   }
   
   rt_make_soft_real_time();
   stop_rt_timer();
   rt_task_delete(task);
   printf("ending...\n");
   setMotorTorque(0.0);
   AnalogOut_Disable();
   rtai_free(nam2num(SHMNAM_ASS), ass);
   close(rtf_fd);

   return 0;
}



