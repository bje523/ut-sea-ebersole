//model based positon (angle) control with disturbance observer

#ifndef _PCONTROL_H
#define _PCONTROL_H

#define PCONTROL_FREQ_HZ 1000 //filter coefficients have to be recalculated if this is changed, actualy frequency determined by FCONTROL_FREQ_HZ/n where n is an integer
#define PCONTROL_PERIOD_SEC (1/(float)PCONTROL_FREQ_HZ)

#define PCONTROL_FC 10 //hz
#define PCONTROL_DOB_FC 9 //hz

#define PCONTROL_WC 2*PI*PCONTROL_FC
#define PCONTROL_DOB_WC 2*PI*PCONTROL_DOB_FC

//default filter coeff values
//P filter
//fc = 10hz
#define P_IN1 33.20028
#define P_IN2 -65.82256
#define P_IN3 32.62227
#define P_OUT1 1.82292
#define P_OUT2 -0.837377

//Pdob filter
//fc = 40hz
#define PDOB_IN1 12.32548
#define PDOB_IN2 -22.46846
#define PDOB_IN3 10.32108
#define PDOB_OUT1 1.3208
#define PDOB_OUT2 -0.4989

//Qpd filter
//fc = 40hz
#define QPD_IN1 0.044527
#define QPD_IN2 0.089054
#define QPD_IN3 0.044527
#define QPD_OUT1 1.3208
#define QPD_OUT2 -0.4989

//call at 1khz
//runs position control loop at a fraction of the call rate
void servoActuatorAngle(float angle_des_rad, float spring_force_n, float arm_angle_rad, float motor_accel, float *force_des, float *motor_torque_des);

#endif //_PCONTROL_H


