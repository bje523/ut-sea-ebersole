#ifndef _ACTUATOR_UTILS_H
#define _ACTUATOR_UTILS_H

#define EN_MIRROR_OUTPUT 1

#define VM_FC 20
#define VA_FC 20
#define AM_FC 5 //changing this will affect stability of force controller
#define FMEAS_FC 20

#define VM_WC 2*PI*VM_FC
#define VA_WC 2*PI*VA_FC
#define AM_WC 2*PI*AM_FC
#define FMEAS_WC 2*PI*FMEAS_FC

//default filter coeff values
//Vm filter
//fc = 20hz
#define VM_IN1 7.225
#define VM_IN2 0.0
#define VM_IN3 -7.225
#define VM_OUT1 1.8229
#define VM_OUT2 -0.83737

//Va filter
//fc = 20hz
#define VA_IN1 7.225
#define VA_IN2 0.0
#define VA_IN3 -7.225
#define VA_OUT1 1.8229
#define VA_OUT2 -0.83737

//A filter
//fc = 5hz
#define A_IN1 965.3
#define A_IN2 -1930.6
#define A_IN3 965.3
#define A_OUT1 1.9556
#define A_OUT2 -0.9565

//Fmeas filter
//fc = 50hz
#define FMEAS_IN1 1518410.7
#define FMEAS_IN2 -2997022.2 
#define FMEAS_IN3 1500600.1
#define FMEAS_OUT1 1.5645
#define FMEAS_OUT2 -0.64366

//sample absolute spring position (analog)
//value is in mm
int sampleSpringPos(float *springPos);

//sample absolute arm angle (analog)
//value is in radians
int sampleArmAngle(float *armAngle);

//read average sensor values (sum and divide)
int getAvgSensorValues(float *avg_spring_pos_mm, float *avg_arm_ang_rad);

//finds initial (absolute) arm angle and initial (absolute) spring deflection
//uses these values to calculate initial actuator length
//return 0 if no error, 1 if sample error
int sensorInit(void);

//write local struct to shared memory
void ass_writeShm(unsigned int fd, Actuator_state_str *Ass, Actuator_state_str *localAss);

//check if arm is within a safe operating region
//false = we're ok
//true = aaah!
int armPosSafetyCheck(float Arm_ang_rad);

//check if arm torque is within a safe operating region
//false = we're ok
//true = aaah!
int armTorqueSafetyCheck(float Arm_torque_nm);

//check if ballscrew force is within a safe operating region
//false = we're ok
//true = aaah!
int armForceSafetyCheck(float Arm_force_n);

//updates the state of the actuator
//requires time since it was last run to calculate velocities (not currently using this, must be called at 1khz)
//safe = 1, check to make sure values are within safe operating levels
//return 0 if safe (or if safe=0), 1 if not
int updateState(Actuator_state_str *localAss, char safe);

//scale and output to motor and mirror channel if enabled
void setMotorTorque(float torque_des);

//if you reset the quad counters in hardware, the incremental spring encoder will develop an offset upon movement
//use this function instead and subtract the measured offset
void quadGetOffset(void);

#endif //_ACTUATOR_UTILS_H





