//sliding mode position control - Ben Ebersole
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include "actuator_utils.h"
#include "smccontrol.h"

static unsigned char FirstRun=1;

static float X_hat = 0.0;
static float V_hat = 0.0;
static float X_hat_dot = 0.0;
static float V_hat_dot = 0.0;

static float Theta_ref = 0.0;
static float Omega_ref = 0.0;
static float Alpha_ref = 0.0;
static float Theta_ref_dot = 0.0;
static float Omega_ref_dot = 0.0;

float commonConstant(len){
  float l_x_ = 1-(-B_LENGTH_M*B_LENGTH_M-C_LENGTH_M*C_LENGTH_M+len*len)*(-B_LENGTH_M*B_LENGTH_M-C_LENGTH_M*C_LENGTH_M+len*len)/(4*B_LENGTH_M*B_LENGTH_M*C_LENGTH_M*C_LENGTH_M);
  return l_x_;
}

float momentArm(ang){
  float L_ = B_LENGTH_M*C_LENGTH_M*sin(ang)/sqrt(B_LENGTH_M*B_LENGTH_M+C_LENGTH_M*C_LENGTH_M-2*B_LENGTH_M*C_LENGTH_M*cos(ang));
  return L_;
}

float actuatorLenSpeed2ArmSpeed(len,vel,l_x){
  float omega_ = len*vel/(B_LENGTH_M*C_LENGTH_M*sqrt(l_x));
  return omega_;
}

float c1_x(len,vel,L,l_x,theta,omega){
  float output_ = -len*(ARM_INERTIA*(vel*vel/(B_LENGTH_M*C_LENGTH_M*sqrt(l_x))
    -len*len*vel*vel/(2*B_LENGTH_M*pow(C_LENGTH_M,3)*pow(l_x,1.5))- 
    len*len*vel*vel/(2*pow(B_LENGTH_M,3)*C_LENGTH_M*pow(l_x,1.5))+ 
    pow(len,4)*vel*vel/(2*pow(B_LENGTH_M,3)*pow(C_LENGTH_M,3)*pow(l_x,1.5)))+ 
    ARM_FRICTION*omega+ARM_MASS*G*COMBINED_ARM_COM_DIST*sin(theta+
    COMBINED_ANG_OFFSET))/(L*SPRUNG_MASS*B_LENGTH_M*C_LENGTH_M*sqrt(l_x)+ARM_INERTIA*len)+ 
    vel*vel/(B_LENGTH_M*C_LENGTH_M*sqrt(l_x))- 
    len*len*vel*vel/(2*B_LENGTH_M*pow(C_LENGTH_M,3)*pow(l_x,1.5))- 
    len*len*vel*vel/(2*pow(B_LENGTH_M,3)*C_LENGTH_M*pow(l_x,1.5))+ 
    pow(len,4)*vel*vel/(2*pow(B_LENGTH_M,3)*pow(C_LENGTH_M,3)*pow(l_x,1.5));
  return output_;
}

float d1_x(len,l_x,L,theta){
  float output_ = len*TORQUE_CONSTANT*BIG_PULLEY_TEETH/SMALL_PULLEY_TEETH*DRIVETRAIN_EFF*2*M_PI/(BALLSCREW_LEAD*(SPRUNG_MASS*B_LENGTH_M*C_LENGTH_M*sqrt(l_x)+ARM_INERTIA*len/L));
  return output_;
}

float smc_u_eq(c1,d1,omega,omega_ref,alpha_ref){
  float u_eq_ = (SMC_CONTROL_SMC_LAMBDA2*(omega_ref-omega)+SMC_CONTROL_SMC_LAMBDA1*alpha_ref-c1)/d1;
  return u_eq_;
}

float smc_u_s(d1,theta,omega,theta_ref,omega_ref){
  float u_s_ = SMC_CONTROL_SMC_ETA*copysign(1,(SMC_CONTROL_SMC_LAMBDA2*(theta_ref-theta)+
    SMC_CONTROL_SMC_LAMBDA1*(omega_ref-omega)))/d1;
  return u_s_;
}

float f_x2(len,vel,theta,omega,L,l_x){
  float output_ = -(ARM_INERTIA*(vel*vel/(B_LENGTH_M*C_LENGTH_M*sqrt(l_x))- 
    len*len*vel*vel/(2*B_LENGTH_M*pow(C_LENGTH_M,3)*pow(l_x,1.5))- 
    len*len*vel*vel/(2*pow(B_LENGTH_M,3)*C_LENGTH_M*pow(l_x,1.5))+ 
    pow(len,4)*vel*vel/(2*pow(B_LENGTH_M,3)*pow(C_LENGTH_M,3)*pow(l_x,1.5)))+ 
    ARM_FRICTION*omega+ 
    ARM_MASS*G*COMBINED_ARM_COM_DIST*sin(theta+COMBINED_ANG_OFFSET))/(L*SPRUNG_MASS+ 
    ARM_INERTIA*len/(B_LENGTH_M*C_LENGTH_M*sqrt(l_x)));
  return output_;
}

float g_x2(len,L,l_x){
  float output_ = (TORQUE_CONSTANT*BIG_PULLEY_TEETH/SMALL_PULLEY_TEETH*DRIVETRAIN_EFF*2*M_PI)/
    (BALLSCREW_LEAD*(SPRUNG_MASS+ARM_INERTIA*len/(L*B_LENGTH_M*C_LENGTH_M*sqrt(l_x))));
  return output_;
}

float servoActuatorAngleSMC(float angle_des_rad, float arm_angle_rad, float dt){
  float torque_motor_des = 0.0;
  static float theta,omega,c1,d1,l_x,L,u_eq,u_s,u;
  if(FirstRun){
	X_hat = SMC_CONTROL_OBS_X0;
	V_hat = SMC_CONTROL_OBS_V0;

	Theta_ref = 0.0;
	Omega_ref = 0.0;
	Alpha_ref = 0.0;	
  }
  else{
    X_hat += X_hat_dot*dt;
    V_hat += V_hat_dot*dt;

    Theta_ref += Theta_ref_dot*dt;
    Omega_ref += Omega_ref_dot*dt;
    Alpha_ref = Omega_ref_dot;
  }

  theta = actuatorLen2ArmAngle(X_hat);
  l_x = commonConstant(X_hat);
  L = momentArm(theta);
  omega = actuatorLenSpeed2ArmSpeed(X_hat,V_hat,l_x);

  c1 = c1_x(X_hat,V_hat,L,l_x,theta,omega);
  d1 = d1_x(X_hat,l_x,L,theta);
  
  u_eq = smc_u_eq(c1,d1,omega,Omega_ref,Alpha_ref);
  u_s = smc_u_s(d1,theta,omega,Theta_ref,Omega_ref);
  u = u_eq+u_s;
  torque_motor_des = u*TORQUE_CONSTANT;
  setMotorTorque(torque_motor_des);

  Theta_ref_dot = Omega_ref;
  Omega_ref_dot = SMC_CONTROL_PF_A*Omega_ref+SMC_CONTROL_PF_B*Theta_ref+
    SMC_CONTROL_PF_C*angle_des_rad;

  X_hat_dot = V_hat + (arm_angle_rad-theta)*SMC_CONTROL_OBS_HE1;
  V_hat_dot = f_x2(X_hat,V_hat,theta,omega,L,l_x)+
    g_x2(X_hat,L,l_x)*u + (arm_angle_rad-theta)*SMC_CONTROL_OBS_HE2;

  FirstRun = 0;
  return torque_motor_des;
}



