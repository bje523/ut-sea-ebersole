//logs actuator data to file
//run during the 10 second wait before test programs starts
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <utils.h>
#include <rtai_shm.h>
#include <rtai_fifos.h>
#include "actuator_params.h"

#define SAMPLE_PERIOD_US 500
#define SAMPLE_DURATION_S 0 
#define LOGFILE_DEFAULT "actuator_data.csv" //tab-separated values
#define FILE_DELIMITER ","
#define CONSOLE_DELIMITER "\t"

static char *LogFile;

static int LogRate = SAMPLE_PERIOD_US;
static int LogDuration = SAMPLE_DURATION_S;
static char File_default[] = LOGFILE_DEFAULT;
static short SuppressConsole = 0;

static int End=0;
static void endme(int dummy) { End=1; }

//shared mem struct
static Actuator_state_str *Ass;

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -r <log rate (us)>      : sample period (default :%d)\n", LogRate);
  printf ("  -d <log duration (s)>   : sample duration, 0 for infinite (default: %d)\n", LogDuration);
  printf ("  -f <log file>           : file to log to (default: %s)\n", File_default);
  printf ("  -s                      : suppress console output\n");
#if RTAI
   printf("RTAI currently enabled (recompile to change)\n");
#else
   printf("RTAI currently disabled (recompile to change)\n");
#endif
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "r:f:d:wsh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'r':
        LogRate = atoi(optarg);
        break;
      case 'f':
        LogFile = optarg;
        break;
      case 'd':
        LogDuration = atoi(optarg);
        break;
      case 's':
        SuppressConsole = 1;
        break;
      case '?': // help
      case 'h': // help
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

//try to update local data from shared memory
//1 if we suceed, 0 if theres no new data yet
unsigned char ass_tryReadShm(unsigned int fd, Actuator_state_str *Ass, Actuator_state_str *localAss){
  unsigned char newData = 0;
  rtf_sem_wait(fd);
  if(Ass->newData){ //update local struct if there is new data
    //printf("new data!\n");
    Ass->newData = 0;
    memcpy((void*)localAss, (const void*)Ass, sizeof(Actuator_state_str));
    newData = 1;
  }
  rtf_sem_post(fd);
  return newData;
}

int main(int argc, char **argv) {

   Actuator_state_str localAss; //contains actuator state data
   unsigned int rtf_fd;

   struct timeval tvStart, tvCurrent;
   long int elapsedTime_us, currentTime_us, sampleTime_prev_us=0;
   double samplePeriod_s;

   int iteration = 0, maxIteration = -1;

   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv);

   Ass = rtai_malloc(nam2num(SHMNAM_ASS), sizeof(Actuator_state_str));
   rtf_fd = open(ASS_FIFO, O_RDWR); //shm access semaphore
   
   //file stuff
   LogFile = File_default;
   FILE* ifp;
   ifp = fopen(LogFile, "w");
   if (!ifp) {
     fprintf(stderr, "Cannot open file %s\n", LogFile);
     exit(1);
   }

   //get starting time
   gettimeofday(&tvStart, NULL);

   if(LogDuration > 0) {
    maxIteration = LogDuration*1000000/LogRate;
   }

   //print header
   printf("Data logging to %s with period = %d us\n", LogFile, LogRate);

#if ENABLE_SIMPLE_DERIVATIVE_MEASUREMENT
   fprintf(ifp, "Time (sec)" FILE_DELIMITER "Sample Period (sec)" FILE_DELIMITER  "Desired Arm Angle(deg)" FILE_DELIMITER  "Arm Angle(deg)" FILE_DELIMITER "Arm Velocity(rad/sec)" FILE_DELIMITER "Spring Force(N)" FILE_DELIMITER "Actuator Force(N)" FILE_DELIMITER "Desired Force(N)" FILE_DELIMITER "Arm Torque (Nm)" FILE_DELIMITER "Motor Torque(Nm)" FILE_DELIMITER "Motor Position(rad)" FILE_DELIMITER "Motor Velocity(rpm)" FILE_DELIMITER "Motor Velocity Simple(rpm)" FILE_DELIMITER "Motor Acceleration(rad/s/s)" FILE_DELIMITER "Motor Acceleration Simple(rad/s/s)" FILE_DELIMITER "Motor Power(W)" FILE_DELIMITER "Arm Power(W)" FILE_DELIMITER "Mechanical Efficiency\n");
#else
   fprintf(ifp, "Time (sec)" FILE_DELIMITER "Sample Period (sec)" FILE_DELIMITER  "Desired Arm Angle(deg)" FILE_DELIMITER  "Arm Angle(deg)" FILE_DELIMITER "Arm Velocity(rad/sec)" FILE_DELIMITER "Spring Force(N)" FILE_DELIMITER "Actuator Force(N)" FILE_DELIMITER "Desired Force(N)" FILE_DELIMITER "Arm Torque (Nm)" FILE_DELIMITER "Motor Torque(Nm)" FILE_DELIMITER "Motor Velocity(rpm)" FILE_DELIMITER "Motor Power(W)" FILE_DELIMITER "Arm Power(W)" FILE_DELIMITER "Mechanical Efficiency\n");
#endif

   while(!End && iteration != maxIteration){

     gettimeofday(&tvCurrent, NULL);
     currentTime_us = tvCurrent.tv_sec*USEC_PER_SEC + tvCurrent.tv_usec;
     elapsedTime_us = currentTime_us - (tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec);
     samplePeriod_s = us2sec(currentTime_us - sampleTime_prev_us);
     sampleTime_prev_us = currentTime_us;
     //printf("sample period %lf\n", samplePeriod_s);

     //update our local state struct and print to file if there is new data
     if(ass_tryReadShm(rtf_fd, Ass, &localAss)){
#if ENABLE_SIMPLE_DERIVATIVE_MEASUREMENT
       fprintf(ifp, "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f\n", localAss.SampleTime_s, localAss.SamplePeriod_s, rad2deg(localAss.Arm_ang_des_rad), rad2deg(localAss.Arm_ang_rad), localAss.Arm_vel_rad, localAss.Spring_force_n, localAss.Actuator_force_n, localAss.Actuator_force_des_n, localAss.Arm_torque_nm, localAss.Motor_torque_des_nm, localAss.Motor_angle_rad, radps2rpm(localAss.Motor_vel_rad), radps2rpm(localAss.Motor_vel_rad_simple), localAss.Motor_acc_rad, localAss.Motor_acc_rad_simple, localAss.Motor_mech_power_w, localAss.Arm_power_w, localAss.Mech_efficiency);
#else
       fprintf(ifp, "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f\n", localAss.SampleTime_s, localAss.SamplePeriod_s, rad2deg(localAss.Arm_ang_des_rad), rad2deg(localAss.Arm_ang_rad), localAss.Arm_vel_rad, localAss.Spring_force_n, localAss.Actuator_force_n, localAss.Actuator_force_des_n, localAss.Arm_torque_nm, localAss.Motor_torque_des_nm, radps2rpm(localAss.Motor_vel_rad), localAss.Motor_mech_power_w, localAss.Arm_power_w, localAss.Mech_efficiency);
#endif
     }

     iteration++;
     usleep(LogRate);
   }

   printf("ending...\n");
   rtai_free(nam2num(SHMNAM_ASS), Ass);
   fclose(ifp);
   close(rtf_fd);

   return 0;
}



