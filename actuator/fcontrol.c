//force feedback control with dynamic compensation
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include "actuator_utils.h"
#include "fcontrol.h"

static float Tdob_in_prev[2] = {0,0};
static float Tdob_out_prev[2] = {0,0};
static float Torque_des_prev=0.0;
static float Tdob_in1=TDOB_IN1, Tdob_in2=TDOB_IN2, Tdob_in3=TDOB_IN3, Tdob_out1=TDOB_OUT1, Tdob_out2=TDOB_OUT2;

static float Qt_in_prev[2] = {0,0};
static float Qt_out_prev[2] = {0,0};
static float Qt_in1=QT_IN1, Qt_in2=QT_IN2, Qt_in3=QT_IN3, Qt_out1=QT_OUT1, Qt_out2=QT_OUT2;

#if FCONTROL_DYNFF_EN
static float Ff_in_prev[3] = {0,0,0};
static float Ff_out_prev[3] = {0,0,0};
static float Ff_in1=FF_IN1, Ff_in2=FF_IN2, Ff_in3=FF_IN3, Ff_in4=FF_IN4, Ff_out1=FF_OUT1, Ff_out2=FF_OUT2, Ff_out3=FF_OUT3;
#endif

static unsigned char FirstRun=1;

//control variables
static float Force_err = 0.0;
static float Force_err_prev = 0.0;
static float Pid_P_gain = P_GAIN_DEFAULT;
static float Pid_I_gain = I_GAIN_DEFAULT;
static float Pid_D_gain = D_GAIN_DEFAULT;
static float Pid_P = 0.0;
static float Pid_I = 0.0;
static float Pid_D = 0.0;

//set pid gains
void fcontrol_init(float p, float i, float d){
  Pid_P_gain = p;
  Pid_I_gain = i;
  Pid_D_gain = d;
}

//generated using gen_tdobfilter.m
static void calculateTdobfilterCoeffs(float w_c, float t_s, float *in1, float*in2, float *in3, float *out1, float *out2){
  float den = motorTorque2BsForce(1)*(2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000);
  *in1 = w_c*w_c*(2500*SPRING_CONST_NpM*t_s*t_s  + 5000*B_FIT*t_s + 10000*M_FIT) / den;
  *in2 = -w_c*w_c*(20000*M_FIT - 5000*SPRING_CONST_NpM*t_s*t_s) / den;
  *in3 = w_c*w_c*(2500*SPRING_CONST_NpM*t_s*t_s  - 5000*B_FIT*t_s + 10000*M_FIT) / den;
  *out1 = motorTorque2BsForce(1)*(20000 - 5000*t_s*t_s*w_c*w_c) / den;
  *out2 = -motorTorque2BsForce(1)*(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;
  //printf("tdob\nf_c:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nout1:%f\nout2:%f\n", w_c/2/PI, t_s, *in1, *in2, *in3, *out1, *out2);
}

//generated using gen_qtfilter.m
static void calculateQtfilterCoeffs(float w_c, float t_s, float *in1, float*in2, float *in3, float *out1, float *out2){
  float den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;
  *in1 = 2500*t_s*t_s*w_c*w_c / den;
  *in2 = 5000*t_s*t_s*w_c*w_c / den;
  *in3 = 2500*t_s*t_s*w_c*w_c / den;
  *out1 = -(5000*t_s*t_s*w_c*w_c  - 20000) / den;
  *out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;
  //printf("qt\nf_c:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nout1:%f\nout2:%f\n", w_c/2/PI, t_s, *in1, *in2, *in3, *out1, *out2);
}


#if FCONTROL_DYNFF_EN
//generated using gen_fffilter.m
static void calculateFFfilterCoeffs(float w_c, float t_s, float *in1, float*in2, float *in3, float *in4, float *out1, float *out2, float *out3){
  float den = motorTorque2BsForce(1)*(t_s*t_s*t_s*w_c*w_c*w_c  + 4*t_s*t_s*w_c*w_c  + 8*t_s*w_c + 8);
  *in1 = w_c*w_c*w_c*(SPRING_CONST_NpM*t_s*t_s*t_s  + 2*B_FIT*t_s*t_s  + 4*M_FIT*t_s) / den;
  *in2 = w_c*w_c*w_c*(3*SPRING_CONST_NpM*t_s*t_s*t_s  + 2*B_FIT*t_s*t_s  - 4*M_FIT*t_s) / den;
  *in3 = -w_c*w_c*w_c*(-3*SPRING_CONST_NpM*t_s*t_s*t_s  + 2*B_FIT*t_s*t_s  + 4*M_FIT*t_s) / den;
  *in4 = w_c*w_c*w_c*(SPRING_CONST_NpM*t_s*t_s*t_s  - 2*B_FIT*t_s*t_s  + 4*M_FIT*t_s) / den;
  *out1 = motorTorque2BsForce(1)*(-3*t_s*t_s*t_s*w_c*w_c*w_c  - 4*t_s*t_s*w_c*w_c  + 8*t_s*w_c + 24) / den;
  *out2 = -motorTorque2BsForce(1)*(3*t_s*t_s*t_s*w_c*w_c*w_c  - 4*t_s*t_s*w_c*w_c  - 8*t_s*w_c + 24) / den;
  *out3 = motorTorque2BsForce(1)*(-t_s*t_s*t_s*w_c*w_c*w_c  + 4*t_s*t_s*w_c*w_c  - 8*t_s*w_c + 8) / den;
  //printf("ff\nf_c:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nin4:%f\nout1:%f\nout2:%f\nout3:%f\n", w_c/2/PI, t_s, *in1, *in2, *in3, *in4, *out1, *out2, *out3);
}
#endif

static float Tdob_filt(float tdob_in){
  float tdob_out = Tdob_in1*tdob_in + Tdob_in2*Tdob_in_prev[0] + Tdob_in3*Tdob_in_prev[1] + //input component
                  Tdob_out1*Tdob_out_prev[0] + Tdob_out2*Tdob_out_prev[1]; //output component
  Tdob_in_prev[1] = Tdob_in_prev[0];
  Tdob_in_prev[0] = tdob_in;
  Tdob_out_prev[1] = Tdob_out_prev[0];
  Tdob_out_prev[0] = tdob_out;
  return tdob_out;                
}

static float Qt_filt(float qt_in){
  float qt_out = Qt_in1*qt_in + Qt_in2*Qt_in_prev[0] + Qt_in3*Qt_in_prev[1] + //input component
                  Qt_out1*Qt_out_prev[0] + Qt_out2*Qt_out_prev[1]; //output component
  Qt_in_prev[1] = Qt_in_prev[0];
  Qt_in_prev[0] = qt_in;
  Qt_out_prev[1] = Qt_out_prev[0];
  Qt_out_prev[0] = qt_out;
  return qt_out;                
}

#if FCONTROL_DYNFF_EN
static float Ff_filt(float ff_in){
  float ff_out = Ff_in1*ff_in + Ff_in2*Ff_in_prev[0] + Ff_in3*Ff_in_prev[1] + Ff_in4*Ff_in_prev[2] + //input component
                  Ff_out1*Ff_out_prev[0] + Ff_out2*Ff_out_prev[1] + Ff_out3*Ff_out_prev[2]; //output component
  Ff_in_prev[2] = Ff_in_prev[1];
  Ff_in_prev[1] = Ff_in_prev[0];
  Ff_in_prev[0] = ff_in;
  Ff_out_prev[2] = Ff_out_prev[1];
  Ff_out_prev[1] = Ff_out_prev[0];
  Ff_out_prev[0] = ff_out;
  return ff_out;                
}
#endif

//performs pid and feed forward control on motor current to servo actuator to desired force
//returns the desired motor torque
float servoActuatorForce(float force_des_n, float spring_force_n, float motor_accel){
  float torque_motor_des=0.0, x_meas_mm, x_des_mm, torque_des_d;
  
  x_meas_mm = springForce_N2springPos_mm(spring_force_n);
  x_des_mm = springForce_N2springPos_mm(force_des_n);
  
  if(FirstRun){
    Tdob_in_prev[0] = x_meas_mm/1000;
    Tdob_in_prev[1] = x_meas_mm/1000;
#if FCONTROL_DYNFF_EN
    Ff_in_prev[0] = x_des_mm/1000;
    Ff_in_prev[1] = x_des_mm/1000;
    Ff_in_prev[2] = x_des_mm/1000;
    calculateFFfilterCoeffs(FCONTROL_WC, FCONTROL_PERIOD_SEC, &Ff_in1, &Ff_in2, &Ff_in3, &Ff_in4, &Ff_out1, &Ff_out2, &Ff_out3);
#endif
	calculateTdobfilterCoeffs(FCONTROL_DOB_WC, FCONTROL_PERIOD_SEC, &Tdob_in1, &Tdob_in2, &Tdob_in3, &Tdob_out1, &Tdob_out2);
	calculateQtfilterCoeffs(FCONTROL_DOB_WC, FCONTROL_PERIOD_SEC, &Qt_in1, &Qt_in2, &Qt_in3, &Qt_out1, &Qt_out2);
  }
   
#if FCONTROL_DYNFF_EN
  torque_motor_des = Ff_filt(x_des_mm/1000); //compensated feed forward torque
#else
  torque_motor_des = BsForce2motorTorque(force_des_n); //uncompensated feed forward torque
#endif
  //fprintf(ifp, "%f,", torque_motor_des);

#if FCONTROL_DOB_EN
  if(FirstRun){
    Torque_des_prev = torque_motor_des;
  }

  torque_des_d = Tdob_filt(x_meas_mm/1000) - Qt_filt(Torque_des_prev); //convert to meters
  torque_motor_des -= torque_des_d; //force system to behave like model
  //fprintf(ifp, "%f,", torque_motor_des);
  Torque_des_prev = torque_motor_des;
#endif

#if FCONTROL_PID_EN
  //calculate pid torque
  Force_err_prev = Force_err;
  Force_err = motorTorque2BsForce(torque_motor_des) - springPos_mm2springForce_N(x_meas_mm);
  Pid_P = Force_err;
  Pid_I += Force_err*FCONTROL_PERIOD_SEC; 
  Pid_D = (Force_err - Force_err_prev)/FCONTROL_PERIOD_SEC;
  torque_motor_des += Pid_P*Pid_P_gain + Pid_I*Pid_I_gain + Pid_D*Pid_D_gain; //add pid torque
#endif

#if FCONTROL_JCOMP_EN
  torque_motor_des += ROTOR_INERTIA*motor_accel; //compensate for effective motor inertia
#endif

  setMotorTorque(torque_motor_des);

  //printf("torque:%f\tdyn_torque:%f\n",torque_motor_des, ROTOR_INERTIA*motor_accel_rad);
  //printf("torque:%f\tdist:%f\n",torque_motor_des, torque_des_d);
  //printf("meas:%f\tdes:%f\n", x_meas_mm_rel_m, x_des_mm_rel_m);
  //printf("torque:%f\n",torque_motor_des);
  
  FirstRun = 0;
  return torque_motor_des;
}




