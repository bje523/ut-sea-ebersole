CC = gcc
CFLAGS = -Wall

all: actuator_utils test_smccontrol measure test_fcontrol test_fresponse test_pcontrol test_presponse data_logger

ACT_UTILS_NAME=actuator_utils
ACT_UTILS_INC=digital_out utils analog_in quadrature analog_out
ACT_UTILS_INC_PARAMS=$(foreach d, $(ACT_UTILS_INC),-I../$d)
ACT_UTILS_RT_INC=-I/usr/realtime/include
ACT_UTILS_OBJS_PARAMS=$(foreach d, $(ACT_UTILS_INC),../$d/$d.o)
ACT_UTILS_LIBS=
ACT_UTILS_SENSITIVITY=$(ACT_UTILS_NAME).c actuator_params.h fcontrol.h
$(ACT_UTILS_NAME): $(ACT_UTILS_SENSITIVITY)
	$(CC) $(CFLAGS) $(ACT_UTILS_INC_PARAMS) $(ACT_UTILS_RT_INC) -c $(ACT_UTILS_NAME).c

MEASURE_NAME=measure
MEASURE_INC=digital_out utils analog_in quadrature analog_out
MEASURE_INC_PARAMS=$(foreach d, $(MEASURE_INC),-I../$d)
MEASURE_RT_INC=-I/usr/realtime/include
MEASURE_OBJS_PARAMS=$(foreach d, $(MEASURE_INC),../$d/$d.o) actuator_utils.o
MEASURE_LIBS=-lm
MEASURE_SENSITIVITY=$(MEASURE_NAME).c actuator_params.h actuator_utils

$(MEASURE_NAME): $(MEASURE_SENSITIVITY)
	$(CC) $(CFLAGS) $(MEASURE_INC_PARAMS) $(MEASURE_RT_INC) -o $(MEASURE_NAME) $(MEASURE_NAME).c $(MEASURE_LIBS) $(MEASURE_OBJS_PARAMS)

FCONTROL_NAME=test_fcontrol
FCONTROL_INC=digital_out utils analog_in quadrature analog_out
FCONTROL_INC_PARAMS=$(foreach d, $(FCONTROL_INC),-I../$d)
FCONTROL_RT_INC=-I/usr/realtime/include
FCONTROL_OBJS_PARAMS=$(foreach d, $(FCONTROL_INC),../$d/$d.o) actuator_utils.o
FCONTROL_SRCS=$(FCONTROL_NAME).c fcontrol.c reference_signals.c
FCONTROL_LIBS=-lm
FCONTROL_SENSITIVITY=$(FCONTROL_NAME).c actuator_params.h actuator_utils fcontrol.c fcontrol.h reference_signals.c reference_signals.h

$(FCONTROL_NAME): $(FCONTROL_SENSITIVITY)
	$(CC) $(CFLAGS) $(FCONTROL_INC_PARAMS) $(FCONTROL_RT_INC) -o $(FCONTROL_NAME) $(FCONTROL_SRCS) $(FCONTROL_LIBS) $(FCONTROL_OBJS_PARAMS)

SMCCONTROL_NAME=test_smccontrol
SMCCONTROL_INC=digital_out utils analog_in quadrature analog_out
SMCCONTROL_INC_PARAMS=$(foreach d, $(SMCCONTROL_INC),-I../$d)
SMCCONTROL_RT_INC=-I/usr/realtime/include
SMCCONTROL_OBJS_PARAMS=$(foreach d, $(SMCCONTROL_INC),../$d/$d.o) actuator_utils.o
SMCCONTROL_SRCS=$(SMCCONTROL_NAME).c smccontrol.c reference_signals.c
SMCCONTROL_LIBS=-lm
SMCCONTROL_SENSITIVITY=$(SMCCONTROL_NAME).c actuator_params.h actuator_utils smccontrol.c smccontrol.h fcontrol.h reference_signals.c reference_signals.h

$(SMCCONTROL_NAME): $(SMCCONTROL_SENSITIVITY)
	$(CC) $(CFLAGS) $(SMCCONTROL_INC_PARAMS) $(SMCCONTROL_RT_INC) -o $(SMCCONTROL_NAME) $(SMCCONTROL_SRCS) $(SMCCONTROL_LIBS) $(SMCCONTROL_OBJS_PARAMS)
	
FRESPONSE_NAME=test_fresponse
FRESPONSE_INC=digital_out utils analog_in quadrature analog_out
FRESPONSE_INC_PARAMS=$(foreach d, $(FRESPONSE_INC),-I../$d)
FRESPONSE_RT_INC=-I/usr/realtime/include
FRESPONSE_OBJS_PARAMS=$(foreach d, $(FRESPONSE_INC),../$d/$d.o) actuator_utils.o
FRESPONSE_SRCS=$(FRESPONSE_NAME).c fcontrol.c reference_signals.c
FRESPONSE_LIBS=-lm
FRESPONSE_SENSITIVITY=$(FRESPONSE_NAME).c actuator_params.h actuator_utils fcontrol.c fcontrol.h reference_signals.c reference_signals.h

$(FRESPONSE_NAME): $(FRESPONSE_SENSITIVITY)
	$(CC) $(CFLAGS) $(FRESPONSE_INC_PARAMS) $(FRESPONSE_RT_INC) -o $(FRESPONSE_NAME) $(FRESPONSE_SRCS) $(FRESPONSE_LIBS) $(FRESPONSE_OBJS_PARAMS)
	
PCONTROL_NAME=test_pcontrol
PCONTROL_INC=digital_out utils analog_in quadrature analog_out
PCONTROL_INC_PARAMS=$(foreach d, $(PCONTROL_INC),-I../$d)
PCONTROL_RT_INC=-I/usr/realtime/include
PCONTROL_OBJS_PARAMS=$(foreach d, $(PCONTROL_INC),../$d/$d.o) actuator_utils.o
PCONTROL_SRCS=$(PCONTROL_NAME).c fcontrol.c pcontrol.c reference_signals.c
PCONTROL_LIBS=-lm
PCONTROL_SENSITIVITY=$(PCONTROL_SRCS) actuator_params.h actuator_utils fcontrol.h pcontrol.h reference_signals.c reference_signals.h

$(PCONTROL_NAME): $(PCONTROL_SENSITIVITY)
	$(CC) $(CFLAGS) $(PCONTROL_INC_PARAMS) $(PCONTROL_RT_INC) -o $(PCONTROL_NAME) $(PCONTROL_SRCS) $(PCONTROL_LIBS) $(PCONTROL_OBJS_PARAMS)

PRESPONSE_NAME=test_presponse
PRESPONSE_INC=digital_out utils analog_in quadrature analog_out
PRESPONSE_INC_PARAMS=$(foreach d, $(PRESPONSE_INC),-I../$d)
PRESPONSE_RT_INC=-I/usr/realtime/include
PRESPONSE_OBJS_PARAMS=$(foreach d, $(PRESPONSE_INC),../$d/$d.o) actuator_utils.o
PRESPONSE_SRCS=$(PRESPONSE_NAME).c fcontrol.c pcontrol.c reference_signals.c
PRESPONSE_LIBS=-lm
PRESPONSE_SENSITIVITY=$(PRESPONSE_SRCS) actuator_params.h actuator_utils fcontrol.h pcontrol.h reference_signals.c reference_signals.h

$(PRESPONSE_NAME): $(PRESPONSE_SENSITIVITY)
	$(CC) $(CFLAGS) $(PRESPONSE_INC_PARAMS) $(PRESPONSE_RT_INC) -o $(PRESPONSE_NAME) $(PRESPONSE_SRCS) $(PRESPONSE_LIBS) $(PRESPONSE_OBJS_PARAMS)

DATALOGGER_NAME=data_logger
DATALOGGER_INC=utils
DATALOGGER_INC_PARAMS=$(foreach d, $(DATALOGGER_INC),-I../$d)
DATALOGGER_RT_INC=-I/usr/realtime/include
DATALOGGER_OBJS_PARAMS=$(foreach d, $(DATALOGGER_INC),../$d/$d.o)
DATALOGGER_LIBS=
DATALOGGER_SENSITIVITY=$(DATALOGGER_NAME).c actuator_params.h

$(DATALOGGER_NAME): $(DATALOGGER_SENSITIVITY)
	$(CC) $(CFLAGS) $(DATALOGGER_INC_PARAMS) $(DATALOGGER_RT_INC) -o $(DATALOGGER_NAME) $(DATALOGGER_NAME).c $(DATALOGGER_LIBS) $(DATALOGGER_OBJS_PARAMS)

clean: 
	rm actuator_utils.o measure test_fcontrol test_fresponse test_pcontrol test_presponse test_smccontrol
