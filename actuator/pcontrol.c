//model based positon (angle) control with disturbance observer
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <ctype.h>
#include <unistd.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include "actuator_utils.h"
#include "fcontrol.h"
#include "pcontrol.h"
#include <digital_out.h>

static float P_in_prev[2] = {0,0};
static float P_out_prev[2] = {0,0};
static float P_in1=P_IN1, P_in2=P_IN2, P_in3=P_IN3, P_out1=P_OUT1, P_out2=P_OUT2;

static float Pdob_in_prev[2] = {0,0};
static float Pdob_out_prev[2] = {0,0};
static float Pdob_in1=PDOB_IN1, Pdob_in2=PDOB_IN2, Pdob_in3=PDOB_IN3, Pdob_out1=PDOB_OUT1, Pdob_out2=PDOB_OUT2;

static float Qpd_in_prev[2] = {0,0};
static float Qpd_out_prev[2] = {0,0};
static float Qpd_in1=QPD_IN1, Qpd_in2=QPD_IN2, Qpd_in3=QPD_IN3, Qpd_out1=QPD_OUT1, Qpd_out2=QPD_OUT2;

static unsigned char FirstRun=1;
static unsigned char ServoCntr=0;
static float Arm_ang_offset;
static float Torque_des_nm=0.0;
static float Ang_des_prev=0.0;
static float Ang_dist=0.0;
static float Ang_des_eff=0.0;

//generated using gen_pfilter.m
static void calculatePfilterCoeffs(float j_a, float b_a, float w_c, float t_s, float *in1, float*in2, float *in3, float *out1, float *out2){
  float den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;
  *in1 = w_c*w_c*(10000*j_a + 5000*b_a*t_s) / den;
  *in2 = -20000*j_a*w_c*w_c / den;
  *in3 = w_c*w_c*(10000*j_a - 5000*b_a*t_s) / den;
  *out1 = -(5000*t_s*t_s*w_c*w_c  - 20000) / den;
  *out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;
  //printf("p\nj_a:%f\nb_a:%f\nf_c:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nout1:%f\nout2:%f\n", j_a, b_a, w_c/2/PI, t_s, *in1, *in2, *in3, *out1, *out2);
}

//generated using gen_pdobfilter.m
static void calculatePdobfilterCoeffs(float wc_p, float wc_pd, float t_s, float *in1, float*in2, float *in3, float *out1, float *out2){
  float den = wc_p*wc_p*(2500*t_s*t_s*wc_pd*wc_pd  + 7071*t_s*wc_pd + 10000);
  *in1 = (2500*t_s*t_s*wc_p*wc_p*wc_pd*wc_pd  + 7071*t_s*wc_p*wc_pd*wc_pd  + 10000*wc_pd*wc_pd) / den;
  *in2 = -(20000*wc_pd*wc_pd  - 5000*t_s*t_s*wc_p*wc_p*wc_pd*wc_pd) / den;
  *in3 =  (2500*t_s*t_s*wc_p*wc_p*wc_pd*wc_pd  - 7071*t_s*wc_p*wc_pd*wc_pd  + 10000*wc_pd*wc_pd) / den;
  *out1 = -wc_p*wc_p*(5000*t_s*t_s*wc_pd*wc_pd  - 20000) / den; //TODO err
  *out2 = -wc_p*wc_p*(2500*t_s*t_s*wc_pd*wc_pd  - 7071*t_s*wc_pd + 10000) / den; //TODO err
  //printf("pdob\nfc_p:%f\nfc_pd:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nout1:%f\nout2:%f\n", wc_p/2/PI, wc_pd/2/PI, t_s, *in1, *in2, *in3, *out1, *out2);
}

//generated using gen_qpdfilter.m
static void calculateQpdfilterCoeffs(float w_c, float t_s, float *in1, float*in2, float *in3, float *out1, float *out2){
  float den = 2500*t_s*t_s*w_c*w_c  + 7071*t_s*w_c + 10000;
  *in1 = 2500*t_s*t_s*w_c*w_c / den;
  *in2 = 5000*t_s*t_s*w_c*w_c / den;
  *in3 = 2500*t_s*t_s*w_c*w_c / den;
  *out1 = -(5000*t_s*t_s*w_c*w_c  - 20000) / den;
  *out2 = -(2500*t_s*t_s*w_c*w_c  - 7071*t_s*w_c + 10000) / den;
  //printf("qpd\nf_c:%f\nt_s:%f\nin1:%f\nin2:%f\nin3:%f\nout1:%f\nout2:%f\n", w_c/2/PI, t_s, *in1, *in2, *in3, *out1, *out2);
}

static float P_filt(float p_in){
  float p_out = P_in1*p_in + P_in2*P_in_prev[0] + P_in3*P_in_prev[1] + //input component
                  P_out1*P_out_prev[0] + P_out2*P_out_prev[1]; //output component
				  
  P_in_prev[1] = P_in_prev[0];
  P_in_prev[0] = p_in;
  P_out_prev[1] = P_out_prev[0];
  P_out_prev[0] = p_out;
  return p_out;                
}

static float Pdob_filt(float pdob_in){
  float pdob_out = Pdob_in1*pdob_in + Pdob_in2*Pdob_in_prev[0] + Pdob_in3*Pdob_in_prev[1] + //input component
                  Pdob_out1*Pdob_out_prev[0] + Pdob_out2*Pdob_out_prev[1]; //output component
				  
  Pdob_in_prev[1] = Pdob_in_prev[0];
  Pdob_in_prev[0] = pdob_in;
  Pdob_out_prev[1] = Pdob_out_prev[0];
  Pdob_out_prev[0] = pdob_out;
  return pdob_out;                
}

static float Qpd_filt(float qpd_in){
  float qpd_out = Qpd_in1*qpd_in + Qpd_in2*Qpd_in_prev[0] + Qpd_in3*Qpd_in_prev[1] + //input component
                  Qpd_out1*Qpd_out_prev[0] + Qpd_out2*Qpd_out_prev[1]; //output component
				  
  Qpd_in_prev[1] = Qpd_in_prev[0];
  Qpd_in_prev[0] = qpd_in;
  Qpd_out_prev[1] = Qpd_out_prev[0];
  Qpd_out_prev[0] = qpd_out;
  return qpd_out;                
}

//call at 1khz
//servos angle at fraction of call rate
void servoActuatorAngle(float angle_des_rad, float spring_force_n, float arm_angle_rad, float motor_accel, float *force_des, float *motor_torque_des){
  float ang_des_rel, ang_meas_rel, ang_des_d;
  //float torque_des_d;

  if(FirstRun){
    Arm_ang_offset = arm_angle_rad;
    calculatePfilterCoeffs(ARM_INERTIA, ARM_FRICTION, PCONTROL_WC, PCONTROL_PERIOD_SEC, &P_in1, &P_in2, &P_in3, &P_out1, &P_out2);
    calculatePdobfilterCoeffs(PCONTROL_WC, PCONTROL_DOB_WC, PCONTROL_PERIOD_SEC, &Pdob_in1, &Pdob_in2, &Pdob_in3, &Pdob_out1, &Pdob_out2);
    calculateQpdfilterCoeffs(PCONTROL_DOB_WC, PCONTROL_PERIOD_SEC, &Qpd_in1, &Qpd_in2, &Qpd_in3, &Qpd_out1, &Qpd_out2);
  }
    
  ServoCntr++;
  if(ServoCntr >= FCONTROL_FREQ_HZ/PCONTROL_FREQ_HZ){
    ServoCntr = 0;

    //we have to servo on relative angle
    ang_des_rel = angle_des_rad - Arm_ang_offset;  
    ang_meas_rel = arm_angle_rad - Arm_ang_offset;

    //pdob
    ang_des_d = Pdob_filt(ang_meas_rel) - Qpd_filt(Ang_des_prev); 
    //printf("ang_meas_rel:%f\tAng_des_prev:%f\tangle_des:%f\tangle_meas:%f\n", rad2deg(ang_meas_rel), rad2deg(Ang_des_prev), rad2deg(angle_des_rad), rad2deg(arm_angle_rad) );
    ang_des_rel -= ang_des_d; //force system to behave like model
    Ang_des_prev = ang_des_rel;
    Ang_dist = ang_des_d;
    Ang_des_eff = ang_des_rel;
  
    //feedforward compensation
    Torque_des_nm = P_filt(ang_des_rel);

    Torque_des_nm -= armGravityTorque(arm_angle_rad); //gravity compensation torque
  }
  //printf("torque_des:%f\tgrav_trq:%f\n",Torque_des_nm, armGravityTorque(arm_angle_rad) );
  //transform to motor torque and send to force controller
  *force_des = armTorque_nm2BsForce_N(Torque_des_nm, arm_angle_rad);
  *motor_torque_des = servoActuatorForce( *force_des, spring_force_n, motor_accel );
  //fprintf(ifp, "%f,%f,%f,", Ang_dist, Ang_des_eff, Torque_des_nm);

  FirstRun = 0;
  return;
}




