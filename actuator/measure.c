//measure state of the actuator
//prints measurements to both the terminal and to file
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h> 
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <signal.h>
#include <analog_in.h>
#include <quadrature.h>
#include <analog_out.h>
#include <utils.h>
#include <math.h>
#include "actuator_params.h"
#include "actuator_utils.h"
#include <rtai_shm.h>
#include <rtai_fifos.h>

#define TOGGLE_DOUT_ON_CYCLE 1 //toggle digital outputs on each cycle (used to measure timing characteristics)

#if TOGGLE_DOUT_ON_CYCLE
#include <digital_out.h>
#endif

#define SAMPLE_PERIOD_MS 100 //10hz
#define SAMPLE_DURATION_S 0 
#define CONSOLE_DELIMITER "\t"

static int LogRate = SAMPLE_PERIOD_MS;
static int LogDuration = SAMPLE_DURATION_S;

static int End=0;
static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -r <log rate (ms)>      : sample period (default :%d)\n", LogRate);
  printf ("  -d <log duration (s)>   : sample duration, 0 for infinite (default: %d)\n", LogDuration);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "r:f:d:wsh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'r':
        LogRate = atoi(optarg);
        break;
      case 'd':
        LogDuration = atoi(optarg);
        break;
      case '?': // help
      case 'h': // help
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args


int main(int argc, char **argv) {

   Actuator_state_str localAss; //contains actuator state data
   Actuator_state_str *ass;
   unsigned int rtf_fd;

   struct timeval tvStart, tvCurrent;
   long int elapsedTime_us, currentTime_us, sampleTime_prev_us=0;
   double samplePeriod_s;

   int iteration = 0, maxIteration = -1;

   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv);

   ass = rtai_malloc(nam2num(SHMNAM_ASS), sizeof(Actuator_state_str));
   rtf_fd = open(ASS_FIFO, O_RDWR); //shm access semaphore
   rtf_sem_init(rtf_fd,1);

   //initialization
   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   AnalogIn_Init();
   Quad_init(512, 20000, 500, 500, 500, 500, 500, 500);
   //Quad_resetCounters();
   quadGetOffset(); //issue with resetting spring incremental encoder, do it this way
#if TOGGLE_DOUT_ON_CYCLE
   DigitalOut_Init();
#endif

   //read absolute positions so we can use relative sensors from here on out
   if(sensorInit()){
     printf("Sample error..ending\n");
     exit(ERR);
   }

   //get starting time
   gettimeofday(&tvStart, NULL);

   if(LogDuration > 0) {
    maxIteration = LogDuration*1000/LogRate;
   }

   //print header
   printf("Arm Angle(deg)" CONSOLE_DELIMITER "Spring Force(N)\n");

   while(!End && iteration != maxIteration){
     gettimeofday(&tvCurrent, NULL);
     currentTime_us = tvCurrent.tv_sec*USEC_PER_SEC + tvCurrent.tv_usec;
     elapsedTime_us = currentTime_us - (tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec);
     samplePeriod_s = us2sec(currentTime_us - sampleTime_prev_us);
     sampleTime_prev_us = currentTime_us;
     //printf("sample period %lf\n", samplePeriod_s);

     localAss.SampleTime_s = us2sec(elapsedTime_us);
     localAss.SamplePeriod_s = samplePeriod_s;
   
     //read actuator state and put in local struct
     if(updateState(&localAss, 0)){
       End = 1;
       break;
     }

     printf("%f" CONSOLE_DELIMITER "%f\n", rad2deg(localAss.Arm_ang_rad), localAss.Spring_force_n);
   
     //write actuator state to shared mem
     ass_writeShm(rtf_fd, ass, &localAss);

     iteration++;
#if TOGGLE_DOUT_ON_CYCLE
     togglePortA(HEARTBEAT_CHAN);
#endif
     usleep(USEC_PER_MSEC*LogRate);
   }

   printf("ending...\n");

   return 0;
}



