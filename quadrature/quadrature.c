//quadrature input driver
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <utils.h>
#include "quadrature.h"

#define RLD 0x80
#define XRLD 0x00
#define YRLD XRLD
#define Rst_BP 0x01
#define Rst_CTR 0x02
#define Rst_FLAGS 0x04
#define Rst_E 0x06
#define Trf_PR_CTR 0x08
#define Trf_CTR_OL 0x10
#define Trf_PR0_PSC 0x18

#define CMR 0xA0
#define XCMR 0x20
#define YCMR XCMR
#define BINCnt 0x00
#define BCDCnt 0x01
#define NrmCnt 0x00
#define RngLmt 0x02
#define NRcyc 0x04
#define ModN 0x06
#define NQDX 0x00
#define QDX1 0x08
#define QDX2 0x10
#define QDX4 0x18

#define IOR 0xC0
#define XIOR 0x40
#define YIOR 0x00
#define DisAB 0x00
#define EnAB 0x01
#define LCTR 0x00
#define LOL 0x02
#define RCTR 0x00
#define ABGate 0x04
#define CYBW 0x00
#define CPBW 0x08
#define CB_UPDN 0x10
#define IDX_ERR 0x18

#define IDR 0xE0
#define XIDR 0x60
#define YIDR XIDR
#define DisIDX 0x00
#define EnIDX 0x01
#define NIDX 0x00
#define PIDX 0x02
#define LIDX 0x00
#define RIDX 0x04

const static unsigned int base = QUAD_BASE_ADDR;
static unsigned int Ticks_per_rev[QUAD_NUM_CHANS];

//initializes quadrature board
//request IO access
//input is ticks per revolution of each quadrature channel
void Quad_init(unsigned int tpr0, unsigned int tpr1, unsigned int tpr2, unsigned int tpr3, unsigned int tpr4, unsigned int tpr5, unsigned int tpr6, unsigned int tpr7){
  int offset=0;
  int i;

  Ticks_per_rev[0] = tpr0;
  Ticks_per_rev[1] = tpr1;
  Ticks_per_rev[2] = tpr2;
  Ticks_per_rev[3] = tpr3;
  Ticks_per_rev[4] = tpr4;
  Ticks_per_rev[5] = tpr5;
  Ticks_per_rev[6] = tpr6;
  Ticks_per_rev[7] = tpr7;
 
#if SHOW_TRANSACTIONS
  printf("Quad: Using base address: 0x%X\n", base);
#endif
  //IOPermission(base, QUAD_ADDR_SZ);

  //outbv(0x01,Select33MFCK(base)); //put CPLD on card into divide-by-1 FCK mode
  outbv(0xFF,IndexInterrupt(base)); //enable index for all channels in CPLD
 
  for(i = 0; i <4; i++){
	  unsigned long XCt = 0xFFFFFF;
	  unsigned long YCt = 0xFFFFFF;

	  //Setup IOR reg
	  outbv(IOR + DisAB + LOL + RCTR + CYBW, CH1_FLGCOMMAND(base+offset));

	  //Setup XPSC
	  outbv(XRLD + Trf_CTR_OL + Rst_BP, CH1_FLGCOMMAND(base+offset));
	  outbv(0x00, CH1_DATA(base+offset));
	  outbv(XRLD + Trf_PR0_PSC + Rst_BP, CH1_FLGCOMMAND(base+offset));

          //Setup YPSC
	  outbv(YRLD + Trf_CTR_OL + Rst_BP, CH2_FLGCOMMAND(base+offset));
	  outbv(0x00, CH2_DATA(base+offset));
	  outbv(YRLD + Trf_PR0_PSC + Rst_BP, CH2_FLGCOMMAND(base+offset));

          //clear flags
          outbv(RLD + Rst_FLAGS, CH1_FLGCOMMAND(base+offset));
          outbv(RLD + Rst_E, CH1_FLGCOMMAND(base+offset));

	  //Setup IDR reg
	  outbv(IDR + EnIDX + NIDX + LIDX, CH1_FLGCOMMAND(base+offset));

	  //Setup CMR reg
	  outbv(CMR + BINCnt + ModN + QDX1, CH1_FLGCOMMAND(base+offset));

	  //Setup PR reg for modulo N counter to XCt
          outbv(XRLD + Trf_CTR_OL + Rst_BP, CH1_FLGCOMMAND(base+offset));
	  outbv(XCt, CH1_DATA(base+offset));
	  XCt >>= 8;
	  outbv(XCt, CH1_DATA(base+offset));
	  XCt >>= 8;
	  outbv(XCt, CH1_DATA(base+offset));

	  //Setup PR reg for modulo N counter to YCt
          outbv(YRLD + Trf_CTR_OL + Rst_BP, CH2_FLGCOMMAND(base+offset));
	  outbv(YCt, CH2_DATA(base+offset));
	  YCt >>= 8;
	  outbv(YCt, CH2_DATA(base+offset));
	  YCt >>= 8;
	  outbv(YCt, CH2_DATA(base+offset));

	  //Enable counters
	  outbv(IOR + EnAB + IDX_ERR, CH1_FLGCOMMAND(base+offset));

          offset += 4;
  }
}

//reset the hardware counters on the pc104 board
void Quad_resetCounters(){
  int offset;
  for(offset=0; offset<4; offset++){
    //reset CNTR and BP
    outbv(RLD + Rst_BP + Rst_CTR, CH1_FLGCOMMAND(base+offset*4));
  }
}

//returns the number of ticks kept by the hardware counters on the pc104 board
long int Quad_getTicks(unsigned int chan)
{
   int addr = base + chan*2;
   union pos_tag {     /* allows access of 32-bit integer as 4 bytes */
   long int l;
   struct byte_tag {char b0; char b1; char b2; char b3;} byte;
   }pos;
   
   if(chan >= 8) {
     printf("Quad_getTicks: invalid channel\n");
     exit(1);
   }

   outbv(XRLD + Trf_CTR_OL + Rst_BP, CH1_FLGCOMMAND(addr));   /* reset address pointer, latch counter */
   pos.byte.b0 = inbv(CH1_DATA(addr));
   pos.byte.b1 = inbv(CH1_DATA(addr));
   pos.byte.b2 = inbv(CH1_DATA(addr));
   /* extend sign of position */  //requires signed characters
   if (pos.byte.b2 < 0) pos.byte.b3 = -1;
   else pos.byte.b3 = 0;

   return pos.l;
}

//get channel angle in radians
double Quad_getAngle_rad(unsigned int chan){
  return QUAD_TICKS2RAD(Quad_getTicks(chan), chan);
}

//get channel angle in degrees
double Quad_getAngle_deg(unsigned int chan){
  return QUAD_TICKS2DEG(Quad_getTicks(chan), chan);
}



