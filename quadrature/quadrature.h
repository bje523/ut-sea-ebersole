#ifndef _QUADRATURE_H
#define _QUADRATURE_H

#define PI 3.14159265

#define QUAD_BASE_ADDR 0x340

#define SHOW_TRANSACTIONS 0
#define QUAD_NUM_CHANS 8

#define CH1_DATA(addr)         ( (addr) + 0x0 )
#define CH1_FLGCOMMAND(addr)   ( (addr) + 0x1 )
#define CH2_DATA(addr)         ( (addr) + 0x2 )
#define CH2_FLGCOMMAND(addr)   ( (addr) + 0x3 )
#define CH3_DATA(addr)         ( (addr) + 0x4 )
#define CH3_FLGCOMMAND(addr)   ( (addr) + 0x5 )
#define CH4_DATA(addr)         ( (addr) + 0x6 )
#define CH4_FLGCOMMAND(addr)   ( (addr) + 0x7 )
#define CH5_DATA(addr)         ( (addr) + 0x8 )
#define CH5_FLGCOMMAND(addr)   ( (addr) + 0x9 )
#define CH6_DATA(addr)         ( (addr) + 0xA )
#define CH6_FLGCOMMAND(addr)   ( (addr) + 0xB )
#define CH7_DATA(addr)         ( (addr) + 0xC )
#define CH7_FLGCOMMAND(addr)   ( (addr) + 0xD )
#define CH8_DATA(addr)         ( (addr) + 0xE )
#define CH8_FLGCOMMAND(addr)   ( (addr) + 0xF )
#define InterruptStatus(addr)  ( (addr) + 0x10 )
#define ChannelOperation(addr) ( (addr) + 0x11 )
#define IndexInterrupt(addr)   ( (addr) + 0x12 )
#define Select130kFCK(addr)    ( (addr) + 0x13 )
#define Select33MFCK(addr)     ( (addr) + 0x14 )

#define QUAD_ADDR_SZ 0x19 //number of IO addresses needed 

#define QUAD_TICKS2RAD(ticks, idx) (((double)(ticks)*2*PI)/(Ticks_per_rev[(idx)]))
#define QUAD_TICKS2DEG(ticks, idx) (((double)(ticks)*360)/(Ticks_per_rev[(idx)]))

//initializes quadrature board
//request IO access
//input is ticks per revolution of each quadrature channel
void Quad_init(unsigned int tpr0, unsigned int tpr1, unsigned int tpr2, unsigned int tpr3, unsigned int tpr4, unsigned int tpr5, unsigned int tpr6, unsigned int tpr7);

//returns the number of ticks kept by the hardware counters on the pc104 board
long int Quad_getTicks(unsigned int chan);

//reset the hardware counters on the pc104 board
void Quad_resetCounters(void);

//get channel angle in radians
double Quad_getAngle_rad(unsigned int chan);

//get channel angle in degrees
double Quad_getAngle_deg(unsigned int chan);


#endif //_QUADRATURE_H
