//quadrature input test code
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <utils.h>
#include "quadrature.h"

int main(void) {
   int ticks=0;
   float curAngle=0.0;
   int i;
   //float dAngle=0.0;
   //float vel_rpm;

   IOPermission(QUAD_BASE_ADDR, QUAD_ADDR_SZ);
   Quad_init(512, 20000, 500, 500, 500, 500, 500, 500);
   Quad_resetCounters();

   while(1){
     for(i =0; i<8; i++){
       //prevAngle = curAngle;
       ticks = Quad_getTicks(i);
       curAngle = Quad_getAngle_deg(i);
       //dAngle = curAngle-prevAngle;
       //vel_rpm = (dAngle)*10*60/360; //change this if the sample period is changed
       //printf("%f %f %f\n",curAngle, prevAngle, dAngle);
       printf("chan %d\tticks %d\tangle: %f\n", i, ticks, curAngle);
     }
     usleep(100000);
     printf("\n");
   }
/*
   while(1){
     prevAngle = curAngle;
     curAngle = Quad_getAngle_deg(0);
     vel_rpm = (curAngle-prevAngle)*10*60/360; //change this if the sample period is changed
     printf("chan %d angle: %f, dangle: %f vel: %f\n", 0, curAngle, curAngle-prevAngle, vel_rpm);
     
     usleep(100000);
     printf("\n");
   }*/
   return 0;
}

/*
//a main function that measures how long it takes to sample an analog channel
int main(void) {
   float adc_voltage;
   struct timeval tv;
   time_t start_time;
   long int diff_time_usec;

   AIO_Init();

   gettimeofday(&tv, NULL);
   start_time=tv.tv_usec;
   (void)sampleADC_5VU(0, &adc_voltage);
   gettimeofday(&tv, NULL);
   diff_time_usec = tv.tv_usec - start_time;
   printf("It took %ld microseconds to sample the ADC\n", diff_time_usec);

   return 0;
}
*/


