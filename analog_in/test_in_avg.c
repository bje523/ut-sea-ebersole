//analog input test code
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <utils.h>
#include "analog_in.h"

#define AVG_SAMPLES 1000

int main(void) {
   float adc_voltage;
   int i=0;
   int numSamples = 0;
   float adc_voltage_avg[8];

   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   AnalogIn_Init();

   while(1){
     i=0;
     //channel 0
     for(i=0; i<8; i++){
       sampleADC_5VU(i, &adc_voltage);
       adc_voltage_avg[i] += adc_voltage;
     }
     numSamples++;
     if(numSamples >= AVG_SAMPLES){
       i=0;
       for(i=0; i<8; i++){
         printf("Avg value for channel %d is %f volts\n", i, adc_voltage_avg[i]/AVG_SAMPLES); 
         adc_voltage_avg[i] = 0;
       }
       numSamples = 0;
       printf("\n");
       usleep(100000); 
     }
   }

   return 0;
}

/*
//a main function that measures how long it takes to sample an analog channel
int main(void) {
   float adc_voltage;
   struct timeval tv;
   time_t start_time;
   long int diff_time_usec;

   AIO_Init();

   gettimeofday(&tv, NULL);
   start_time=tv.tv_usec;
   (void)sampleADC_5VU(0, &adc_voltage);
   gettimeofday(&tv, NULL);
   diff_time_usec = tv.tv_usec - start_time;
   printf("It took %ld microseconds to sample the ADC\n", diff_time_usec);

   return 0;
}
*/


