#ifndef _ANALOG_IN_H
#define _ANALOG_IN_H

#ifndef AIO_BASE_ADDR
#define AIO_BASE_ADDR 0x300
#endif

#define ADC_PRECISION 4096

#define Status(addr)    ( (addr) + 0x0 )
#define Interrupt(addr) ( (addr) + 0x1 )
#define ADC(addr)       ( (addr) + 0x2 )
#define ADC_H(addr)     ( (addr) + 0x3 )

#ifndef AIO_ADDR_SZ
#define AIO_ADDR_SZ 0x19 //number of IO addresses needed 
#endif

enum{ERR_NONE, ERR};

//inputs: ADC channel
// pol: 0=unipolar, 1=bipolar
// range: 0=5V, 1=10V
//output: ADC channel value (raw value, 12-bit)
//takes about 34usec to run
int sampleADC_raw2(uint8_t chan, int *value, int pol, int range);

//sample 5V unipolar
int sampleADC_raw(uint8_t chan, int *value);

//returns an analog channel (0-5 volts), takes about 60usec to run
int sampleADC_5VU(uint8_t chan, float *voltage);

//returns an analog channel (0-10 volts), takes about 60usec to run
int sampleADC_10VU(uint8_t chan, float *voltage);

//returns an analog channel (-5->+5 volts), takes about 60usec to run
int sampleADC_5VB(uint8_t chan, float *voltage);

//returns an analog channel (-10->+10 volts), takes about 60usec to run
int sampleADC_10VB(uint8_t chan, float *voltage);

//initializes AIO board for analog sampling
void AnalogIn_Init(void);



#endif //_ANALOG_IN_H
