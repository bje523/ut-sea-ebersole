//analog input driver
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <utils.h>
#include "analog_in.h"

static unsigned int base = AIO_BASE_ADDR;

volatile union {
   uint8_t value;
   struct {
     unsigned :2;
     unsigned IRQ_EN :1;
     unsigned :3;
     unsigned C_COS :1;
     unsigned ADC_EOC :1;
   } bits;
} AIO_Status;

volatile union {
   uint8_t value;
   struct {
     unsigned ZERO :2;
     unsigned IRQ_EVENT :1;
     unsigned DIO_C0_IRQ :1;
     unsigned DIO_C3_IRQ :1;
     unsigned COUNTER1_IRQ :1;
     unsigned CHANGE_OF_STATE_IRQ :1;
     unsigned ADC_IRQ :1;
   } bits;
} AIO_Interrupt_r;

volatile union {
   uint8_t value;
   struct {
     unsigned :2;
     unsigned IRQ_EN :1;
     unsigned DIO_C0_IRQ_EN :1;
     unsigned DIO_C3_IRQ_EN :1;
     unsigned COUNTER1_IRQ_EN :1;
     unsigned CHANGE_OF_STATE_IRQ_EN :1;
     unsigned ADC_IRQ_EN :1;
   } bits;
} AIO_Interrupt_w;

volatile union {
   uint8_t value;
   struct {
     unsigned CHAN :3;
     unsigned POL :1;
     unsigned RANGE :1;
     unsigned AQ_MODE :1;
     unsigned DEV_MODE :2;
   } bits;
} AIO_ADC;

//sample 5V unipolar
int sampleADC_raw(uint8_t chan, int *value){
  return sampleADC_raw2(chan, (int*)value, 0, 0);
}

//inputs: ADC channel
// pol: 0=unipolar, 1=bipolar
// range: 0=5V, 1=10V
//output: ADC channel value (raw value, 12-bit)
//takes about 34usec to run
int sampleADC_raw2(uint8_t chan, int *value, int pol, int range){
   int timeout_cntr=0;
   unsigned char sampleReceived=0, sampleComplete=0;
   
   AIO_ADC.bits.CHAN = chan;
   AIO_ADC.bits.POL = pol;
   AIO_ADC.bits.RANGE = range;
   AIO_ADC.bits.AQ_MODE = 0;
   AIO_ADC.bits.DEV_MODE = 0;

   outbv(AIO_ADC.value,ADC(base)); //start A/D conversion

   while(!sampleComplete){
     AIO_Interrupt_r.value = inbv(Interrupt(base));
     timeout_cntr++;
     if(timeout_cntr > 100){
       return ERR; //timeout
     }
     if(!sampleReceived){
       if(AIO_Interrupt_r.bits.ADC_IRQ == 1) { //see if theres an ADC interrupt
         *value = inwv(ADC(base)); //read 12-bit ADC value
         outbv(0,Status(base)); //clear IRQ
         AIO_Status.value = inbv(Status(base)); //clear event bits
         sampleReceived = 1;
       }
     }
     else{ //we have received a sample, wait for the interrupt to go away before leaving
       if(AIO_Interrupt_r.bits.ADC_IRQ == 0) { //no more ADC interrupt
         sampleComplete = 1;
       }
     }
   }
   return ERR_NONE;
}

//returns an analog channel (0-5 volts), takes about 60usec to run
int sampleADC_5VU(uint8_t chan, float *voltage){
  int err;
  int value_raw;
  err = sampleADC_raw2(chan, &value_raw, 0, 0);
  err = sampleADC_raw2(chan, &value_raw, 0, 0); //for some reason we don't get the right value if we dont run this twice
  *voltage = ((float)value_raw * 5.0) / ADC_PRECISION;
  return err;
}

//returns an analog channel (0-10 volts), takes about 60usec to run
int sampleADC_10VU(uint8_t chan, float *voltage){
  int err;
  int value_raw;
  err = sampleADC_raw2(chan, &value_raw, 0, 1);
  err = sampleADC_raw2(chan, &value_raw, 0, 1); //for some reason we don't get the right value if we dont run this twice
  *voltage = ((float)value_raw * 10.0) / ADC_PRECISION;
  return err;
}

//returns an analog channel (-5->+5 volts), takes about 60usec to run
int sampleADC_5VB(uint8_t chan, float *voltage){
  int err;
  int value_raw;
  err = sampleADC_raw2(chan, &value_raw, 1, 0);
  err = sampleADC_raw2(chan, &value_raw, 1, 0); //for some reason we don't get the right value if we dont run this twice
  *voltage = ((float)value_raw * 5.0) / ADC_PRECISION;
  return err;
}

//returns an analog channel (-10->+10 volts), takes about 60usec to run
int sampleADC_10VB(uint8_t chan, float *voltage){
  int err;
  int value_raw;
  err = sampleADC_raw2(chan, &value_raw, 1, 1);
  err = sampleADC_raw2(chan, &value_raw, 1, 1); //for some reason we don't get the right value if we dont run this twice
  *voltage = ((float)value_raw * 10.0) / ADC_PRECISION;
  return err;
}

//initializes AIO board for analog sampling
void AnalogIn_Init(void){
#if SHOW_TRANSACTIONS
  printf("AIO: Using base address: 0x%X\n", base);
#endif
  //IOPermission(base, AIO_ADDR_SZ);

  AIO_Status.value = 0;
  AIO_Interrupt_r.value = 0;
  AIO_Interrupt_w.value = 0;
  AIO_ADC.value = 0;

  AIO_Interrupt_w.bits.IRQ_EN=1; //enable global interrupts
  AIO_Interrupt_w.bits.ADC_IRQ_EN=1; //enable ADC interrupts

  outbv(AIO_Interrupt_w.value,Interrupt(base));//set up IRQ events
}

/* int main(void) {
   float adc_voltage;

   AnalogIn_Init();

   while(1){
     for(int i=0; i<8; i++){
       if( sampleADC_5VU(i, &adc_voltage) == ERR_NONE ) {
         printf("Value for channel %d is %f volts\n", i, adc_voltage); 
       }
       else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
    }
     printf("\n");
     usleep(100000);
   }

   return 0;
} */

/*
//a main function that measures how long it takes to sample an analog channel
int main(void) {
   float adc_voltage;
   struct timeval tv;
   time_t start_time;
   long int diff_time_usec;

   AIO_Init();

   gettimeofday(&tv, NULL);
   start_time=tv.tv_usec;
   (void)sampleADC_5VU(0, &adc_voltage);
   gettimeofday(&tv, NULL);
   diff_time_usec = tv.tv_usec - start_time;
   printf("It took %ld microseconds to sample the ADC\n", diff_time_usec);

   return 0;
}
*/


