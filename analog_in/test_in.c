//analog input test code
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <unistd.h>
#include <utils.h>
#include "analog_in.h"

int main(void) {
   float adc_voltage;
   int i=0;

   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   AnalogIn_Init();

   while(1){
     i=0;
     //channel 0
     if( sampleADC_5VU(i, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", i, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
     i++;
     //channel 1
     if( sampleADC_5VU(i, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", i, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
     i++;
     //channel 2
     if( sampleADC_5VU(i, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", i, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
     i++;
     //channel 3
     if( sampleADC_5VU(i, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", i, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
     i++;
     //channel 4 //BIPOLAR!!!
     if( sampleADC_5VB(i, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", i, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
     i++;
     //channel 5
     if( sampleADC_5VU(i, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", i, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
     i++;
     //channel 6
     if( sampleADC_5VU(i, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", i, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
     i++;
     //channel 7
     if( sampleADC_5VU(i, &adc_voltage) == ERR_NONE ) {
       printf("Value for channel %d is %f volts\n", i, adc_voltage); 
     }
     else { printf("Error reading from channel %d (no response from A/D board)\n",0); }
     printf("\n");
     usleep(100000);
   }

   return 0;
}

/*
//a main function that measures how long it takes to sample an analog channel
int main(void) {
   float adc_voltage;
   struct timeval tv;
   time_t start_time;
   long int diff_time_usec;

   AIO_Init();

   gettimeofday(&tv, NULL);
   start_time=tv.tv_usec;
   (void)sampleADC_5VU(0, &adc_voltage);
   gettimeofday(&tv, NULL);
   diff_time_usec = tv.tv_usec - start_time;
   printf("It took %ld microseconds to sample the ADC\n", diff_time_usec);

   return 0;
}
*/


