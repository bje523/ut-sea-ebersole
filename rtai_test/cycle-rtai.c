//timing code for rtai timing functions
//toggles all ports high and low
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <signal.h>

#include <rtai_msg.h>

#include <utils.h>
#include <digital_out.h>

#define CYCLE_RATE_HZ 1000
#define NS_in_SEC 1000000000
#define RTAI_TASK_NAME "CYCLE" //must be unique

#define USEC_PER_SEC 1000000
#define USEC_PER_MSEC 1000
#define NS_PER_SEC 1000000000

#define ns2sec(ns) ((double)(ns)/(double)NS_PER_SEC)
#define us2sec(ns) ((double)(ns)/(double)USEC_PER_SEC)

#define LOGFILE_DEFAULT "timing_data.csv"
#define FILE_DELIMITER ","

static char File_default[] = LOGFILE_DEFAULT;
static char *LogFile;

static int End=0;

static void endme(int dummy) { End=1; }

int main (int argc, char **argv)
{
   RT_TASK *task;
   int period;

   RTIME start_time, elapsedTime_ns, sampleTime_prev_ns=0, currentTime_ns;
   double samplePeriod_s;
   float elapsedTime_s;

   //file stuff
   LogFile = File_default;
   FILE* ifp;
   ifp = fopen(LogFile, "w");
   if (!ifp) {
     fprintf(stderr, "Cannot open file %s\n", LogFile);
     exit(1);
   }
   fprintf(ifp, "Time (sec)" FILE_DELIMITER "Sample Period\n");

   signal(SIGINT, endme); //ctrl-c out gracefully

   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   DigitalOut_Init();

   //schedule control task
   if (!(task = rt_task_init_schmod(nam2num(RTAI_TASK_NAME), 0, 0, 0, SCHED_FIFO, 0xF))) {
     printf("CANNOT INIT %s TASK\n", RTAI_TASK_NAME);
     exit(1);
   }
   //rt_set_oneshot_mode();
   rt_set_periodic_mode();
   period = start_rt_timer(nano2count(NS_in_SEC/CYCLE_RATE_HZ));
   mlockall(MCL_CURRENT | MCL_FUTURE); //keep program from paging
   rt_make_hard_real_time();
   rt_task_make_periodic(task, rt_get_time() + period, period);

   printf("Toggling all digital outputs at %d hz\n", CYCLE_RATE_HZ);
   fflush(stdout);

   //get starting time
   start_time = rt_get_time_ns();
   sampleTime_prev_ns = start_time;
 
   while (!End) {

     //timing
     currentTime_ns = rt_get_time_ns();
     elapsedTime_ns = currentTime_ns - start_time;
     samplePeriod_s = ns2sec(currentTime_ns - sampleTime_prev_ns);
     sampleTime_prev_ns = currentTime_ns;
     elapsedTime_s = ns2sec(elapsedTime_ns);

     //fprintf(ifp, "%f" FILE_DELIMITER "%f\n", elapsedTime_s, samplePeriod_s);
     if(samplePeriod_s > 0.0015) printf(":( %f\t%f\n", samplePeriod_s, elapsedTime_s);

     togglePortA(0xFF);
     togglePortB(0xFF);
     togglePortC(0xFF);
     rt_task_wait_period();
   }

   rt_make_soft_real_time();
   stop_rt_timer();
   rt_task_delete(task);
   printf(" ending..\n");
   fclose(ifp);

   return 0;
}

