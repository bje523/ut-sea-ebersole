//semaphore tests
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <signal.h>
#include <rtai_fifos.h>

int main (int argc, char **argv)
{
   unsigned int fd;
   fd = open("/dev/rtf0", O_RDWR);
   rtf_sem_post(fd);
   close(fd);

   return 0;
}

