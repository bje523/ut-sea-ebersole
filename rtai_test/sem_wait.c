//semaphore tests

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <signal.h>
#include <rtai_fifos.h>

static int End=0;
static void endme(int dummy) { End=1; }

int main (int argc, char **argv)
{
   unsigned int fd;
   fd = open("/dev/rtf0", O_RDWR);
   rtf_sem_init(fd,1);

   signal(SIGINT, endme); //ctrl-c out gracefully
 
   while (!End) {
     printf("waiting...\n");
     rtf_sem_wait(fd);
     printf("sig received\n");
     usleep(1000);
   }
   close(fd);
   printf(" ending..\n");

   return 0;
}

