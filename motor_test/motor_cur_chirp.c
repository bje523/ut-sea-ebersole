//sinusoidal current output with logging
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <analog_out.h>
#include <math.h>
#include <utils.h>

//analog output channels
#define MOTOR_CHAN 1
#define MIRROR_CHAN 2

#define PI 3.14159265

#define CURRENT_OFFSET_ADJUST 0.1658 //Amps
#define MOTOR_TORQUE_DIRECTION -1
#define EN_MIRROR_OUTPUT 1
#define DAC_VOLTAGE 10.0
#define ELMO_PEAK_CURRENT 30.0
#define currentOut2dacVoltage(current) ((float)(current)*DAC_VOLTAGE/ELMO_PEAK_CURRENT)

//chirp params
#define SWEEP_FREQ_HZ_LOW 0.001
#define SWEEP_FREQ_HZ_HIGH 50
#define SWEEP_RATE 0.033 //percent change of sweep range per second
#define SWEEP_RANGE (SWEEP_FREQ_HZ_HIGH-SWEEP_FREQ_HZ_LOW)
#define SWEEP_SETPOINT_CURRENT_AMPLITUDE_A_DEFAULT 1.0
#define SWEEP_SETPOINT_MID_CURRENT_A 0.0 

#define SAMPLE_PERIOD_US 2000
#define SAMPLE_DURATION_S 0 
#define LOGFILE_DEFAULT "motor_chirp_data.csv" //comma-separated values
#define FILE_DELIMITER ","
#define CONSOLE_DELIMITER "\t"

static char *LogFile;
FILE* ifp;
static int LogRate = SAMPLE_PERIOD_US;
static char File_default[] = LOGFILE_DEFAULT;

static float Switching_amp_amps = SWEEP_SETPOINT_CURRENT_AMPLITUDE_A_DEFAULT;
static float Switching_offset_amps = SWEEP_SETPOINT_MID_CURRENT_A;
static float Prev_effective_angle = 0.0;

static unsigned char FirstRun=1;

static unsigned char End=0;

static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -a <current>       : set chirp amplitude (default: %f)\n", Switching_amp_amps);
  printf ("  -o <current>       : set chirp offset (default: %f)\n", Switching_offset_amps);
  printf ("  -r <log rate (us)> : sample period (default :%d)\n", LogRate);
  printf ("  -f <log file>      : file to log to (default: %s)\n", File_default);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "a:o:h?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'a': 
          Switching_amp_amps = atof(optarg);
          break;
      case 'o': 
          Switching_offset_amps = atof(optarg);
          break;
      case 'r':
        LogRate = atoi(optarg);
        break;
      case 'f':
        LogFile = optarg;
        break;
      case '?': // help
      case 'h': // help
      case ':':
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

//scale and output to motor and mirror channel if enabled
void setMotorCurrent(float current_des_A){
  float motor_current_adj;
  motor_current_adj = MOTOR_TORQUE_DIRECTION*current_des_A - CURRENT_OFFSET_ADJUST;
  outputDAC_10VB(MOTOR_CHAN, currentOut2dacVoltage(motor_current_adj));
#if EN_MIRROR_OUTPUT
  outputDAC_10VB(MIRROR_CHAN, currentOut2dacVoltage(motor_current_adj));
#endif
}

float getChirpSignal(float elapsedTime, float samplePeriod, float switching_amp_amps, float switching_offset_amps, unsigned char *end){
  float amplitude, offset, current_des_amps=0.0, effective_switching_freq_hz=0.0, effective_angle;
  amplitude = switching_amp_amps;
  offset = switching_offset_amps;
  
  if(elapsedTime > 0.0){
    
    //linear chirp
    //effective_switching_freq_hz = SWEEP_FREQ_HZ_LOW + SWEEP_RATE*SWEEP_RANGE*t;
    //current_des_amps = amplitude*sin(t * 2 * PI * effective_switching_freq_hz ) + offset;

    //exponential chirp
    effective_angle = 2 * PI * SWEEP_FREQ_HZ_LOW * (pow(SWEEP_RATE*SWEEP_RANGE,elapsedTime)-1)/log(SWEEP_RATE*SWEEP_RANGE);
    current_des_amps = amplitude*sin(effective_angle) + offset;
     
    if(samplePeriod <= 0.0){
      effective_switching_freq_hz = 0.0;
    }
    else{
      effective_switching_freq_hz = (effective_angle - Prev_effective_angle)/samplePeriod/2/PI; 
    }
    Prev_effective_angle = effective_angle;
//    fprintf(ifp, "%f" FILE_DELIMITER, effective_switching_freq_hz);
//    printf("%f\n", effective_switching_freq_hz);
    if(effective_switching_freq_hz > SWEEP_FREQ_HZ_HIGH) {
      *end = 1;
    }
  }
  else{
    current_des_amps = offset;
  }
  return current_des_amps;
}

int main(int argc, char **argv) {

   float current_des = 0.0; //desired current (+- 30amps)

   struct timeval tvStart, tvCurrent;
   long int elapsedTime_us, currentTime_us, sampleTime_prev_us=0;
   double samplePeriod_s, elapsedTime_s;
   gettimeofday(&tvStart, NULL);
   //printf("%ld.%06ld\n", tvBegin.tv_sec, tvBegin.tv_usec);

   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv); //check command line params

   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   AnalogOut_Init(); //request port access
   AnalogOut_Zero(); //sets all outputs to zero value (check jumpers/manual for what voltage this corresponds to)
   AnalogOut_Enable(); //enables analog outputs
   
   //file stuff
   LogFile = File_default;
   //FILE* ifp;
   ifp = fopen(LogFile, "w");
   if (!ifp) {
     fprintf(stderr, "Cannot open file %s\n", LogFile);
     exit(1);
   }
   
   //print header
   printf("Data logging to %s with period = %d us\n", LogFile, LogRate);
   fprintf(ifp, "Time (sec)" FILE_DELIMITER "Sample Period (sec)" FILE_DELIMITER "Desired Current (A)\n");

   
   printf("Starting. Using internal current offset of %f amps. Use -? for command listing.\n", CURRENT_OFFSET_ADJUST);

   while(!End){
     gettimeofday(&tvCurrent, NULL);
     currentTime_us = tvCurrent.tv_sec*USEC_PER_SEC + tvCurrent.tv_usec;
     elapsedTime_us = currentTime_us - (tvStart.tv_sec*USEC_PER_SEC + tvStart.tv_usec);
     elapsedTime_s = us2sec(elapsedTime_us);
     if(FirstRun){
       sampleTime_prev_us = currentTime_us;
       FirstRun = 0;
     }
     samplePeriod_s = us2sec(currentTime_us - sampleTime_prev_us);
     sampleTime_prev_us = currentTime_us;
     current_des = getChirpSignal(elapsedTime_s, samplePeriod_s, Switching_amp_amps, Switching_offset_amps, &End);
     fprintf(ifp, "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f\n", elapsedTime_s, samplePeriod_s, current_des);
     printf("desired current: %f amps\n", current_des);
     setMotorCurrent(current_des);
     usleep(LogRate);
   }

   printf("Ending..\n");
   setMotorCurrent(0.0);
   AnalogOut_Disable();
   fclose(ifp);

   return 0;
}



