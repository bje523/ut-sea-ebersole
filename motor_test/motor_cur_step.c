//sinusoidal current output
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <sys/time.h>
#include <unistd.h>
#include <ctype.h>
#include <signal.h>
#include <analog_out.h>
#include <math.h>
#include <utils.h>

//analog output channels
#define MOTOR_CHAN 1
#define MIRROR_CHAN 2

#define USEC_PER_SEC 1000000

#define CURRENT_OFFSET_ADJUST 0.1658 //Amps
#define MOTOR_TORQUE_DIRECTION -1
#define EN_MIRROR_OUTPUT 1
#define DAC_VOLTAGE 10.0
#define ELMO_PEAK_CURRENT 30.0
#define currentOut2dacVoltage(current) ((float)(current)*DAC_VOLTAGE/ELMO_PEAK_CURRENT)

#define AMPLITUDE_DEFAULT 0.0

static int End=0;
static float Amplitude=AMPLITUDE_DEFAULT;

static void endme(int dummy) { End=1; }

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -a <current>  : set current amplitude (default: %f)\n", AMPLITUDE_DEFAULT);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "a:h?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'a': 
          Amplitude = atof(optarg);
          break;
      case '?': // help
      case 'h': // help
      case ':':
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

//scale and output to motor and mirror channel if enabled
void setMotorCurrent(float current_des_A){
  float motor_current_adj;
  motor_current_adj = MOTOR_TORQUE_DIRECTION*current_des_A - CURRENT_OFFSET_ADJUST;
  outputDAC_10VB(MOTOR_CHAN, currentOut2dacVoltage(motor_current_adj));
#if EN_MIRROR_OUTPUT
  outputDAC_10VB(MIRROR_CHAN, currentOut2dacVoltage(motor_current_adj));
#endif
}

int main(int argc, char **argv) {
   signal(SIGINT, endme); //ctrl-c out gracefully
   parse_args(argc,argv); //check command line params

   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   AnalogOut_Init(); //request port access
   AnalogOut_Zero(); //sets all outputs to zero value (check jumpers/manual for what voltage this corresponds to)
   AnalogOut_Enable(); //enables analog outputs
   
   printf("Starting. Using internal current offset of %f amps. Use -? for command listing.\n", CURRENT_OFFSET_ADJUST);
   printf("desired current: %f amps\n", Amplitude);
   setMotorCurrent(Amplitude);
   
   while(!End){
     usleep(2000);
   }

   printf("Ending..\n");
   setMotorCurrent(0.0);
   AnalogOut_Disable();

   return 0;
}



