#ifndef _UTILS_H
#define _UTILS_H

#define SHOW_TRANSACTIONS 0

#define PI 3.14159265

#define USEC_PER_SEC 1000000
#define USEC_PER_MSEC 1000
#define NS_PER_SEC 1000000000

#define ns2sec(ns) ((double)(ns)/(double)NS_PER_SEC)
#define us2sec(ns) ((double)(ns)/(double)USEC_PER_SEC)

//request IO access
void IOPermission(unsigned int abase, unsigned int asize);

//verbose outb
void outbv(int value, int addr);

//verbose outw
void outwv(int value, int addr);

//verbose inb
int inbv(int addr);

//verbose inw
int inwv(int addr);

#endif //_UTILS_H
