#include <sys/io.h>
#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

//request IO access
void IOPermission(unsigned int abase, unsigned int asize){
    if(ioperm(abase,asize,1)<0)
    {
	fprintf(stderr, "NO IO PERMISSION\n");
	    exit(EXIT_FAILURE);
    }//if
}//IOPermission

//verbose outb
void outbv(int value, int addr){
#if SHOW_TRANSACTIONS
  printf("Writing %X to address %X\n",value, addr);
#endif
  outb(value, addr);
}

//verbose inb
int inbv(int addr){
  int value;
  value = inb(addr);
#if SHOW_TRANSACTIONS
  printf("Read %X from address %X\n",value, addr);
#endif
  return value;
}

//verbose outw
void outwv(int value, int addr){
#if SHOW_TRANSACTIONS
  printf("Writing %X to address %X\n",value, addr);
#endif
  outw(value, addr);
}

//verbose inw
int inwv(int addr){
  int value;
  value = inw(addr);
#if SHOW_TRANSACTIONS
  printf("Read %X from address %X\n",value, addr);
#endif
  return value;
}
