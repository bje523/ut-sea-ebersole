DIRS = utils analog_in analog_out digital_out quadrature spring_measurement motor_test actuator

default: all

all:
	for d in $(DIRS); do $(MAKE) -C $$d ; done

clean:
	for d in $(DIRS); do $(MAKE) -C $$d clean; done
