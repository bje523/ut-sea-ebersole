//measure force vs displacement
//prints measurements to both the terminal and to file
/********************************************
must be in su mode for ioperm to work
*********************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/io.h>
#include <ctype.h>
#include <sys/time.h>
#include <signal.h>
#include <analog_in.h>
#include <utils.h>

//analog input channels
#define FORCE_CHAN 0
#define DISPLACEMENT_CHAN 1

//spring measurement force constants
#define SM_F_N_PER_V 501034.0
#define SM_F_GAIN 692
#define SM_F_V_NOLOAD 0.694
#define SM_F_V_OFFSET ((SM_F_V_NOLOAD)) //-0.012)

//spring measurement position constants
#define SM_P_LV 0.0 
#define SM_P_HV 5.0 
#define SM_P_VDIFF (SM_P_HV-SM_P_LV)
#define SM_P_ETRAVEL_MM 50.0
#define SM_P_POT2PLATE 9.8 //position offset between position at the sensor and position at the plates

//unit conversion
#define voltage2ForceN(voltage) (((voltage)-SM_F_V_OFFSET)*SM_F_N_PER_V/SM_F_GAIN)
#define voltage2PosMM(voltage) (((voltage)*SM_P_ETRAVEL_MM)/SM_P_VDIFF + SM_P_POT2PLATE)
#define mm2in(mm) ((mm)*0.0393700787 )


static char File_default[] = "measure_data.tsv";
static char *printFile;
static int End=0;

static void endme(int dummy) { End=1; }

//print header
static void printRowOne(FILE* ifp){
  char rowOne[] = "Force (N)\tDisplacement (mm)\n";
  printf("%s", rowOne);
  fprintf(ifp, "%s", rowOne);
}

int main(void) {
   float adc_voltage, force_N, cur_position_mm, init_position_mm, displacement_mm;
   printFile = File_default;
   signal(SIGINT, endme); //ctrl-c out gracefully
   
   //open file
   FILE* ifp;
   ifp = fopen(printFile, "w");
   if (!ifp) {
     fprintf(stderr, "Cannot open file %s\n", printFile);
     exit(1);
   }

   IOPermission(AIO_BASE_ADDR, AIO_ADDR_SZ);
   AnalogIn_Init();

   printRowOne(ifp);

   //sample initial position
   if( sampleADC_5VU(DISPLACEMENT_CHAN, &adc_voltage) == ERR_NONE ) {
       init_position_mm = voltage2PosMM(adc_voltage);
     }

   while(!End){
     if( sampleADC_5VU(FORCE_CHAN, &adc_voltage) == ERR_NONE ) {
       force_N = voltage2ForceN(adc_voltage);
       printf("%f\t", force_N); 
       fprintf(ifp, "%f\t", force_N); 
     }
     if( sampleADC_5VU(DISPLACEMENT_CHAN, &adc_voltage) == ERR_NONE ) {
       cur_position_mm = voltage2PosMM(adc_voltage);
       displacement_mm = init_position_mm - cur_position_mm;
       printf("%f (spring @ %f in)\n", displacement_mm, mm2in(cur_position_mm)); 
       fprintf(ifp, "%f\n", displacement_mm); 
     }
     usleep(100000); //10hz 
   }
   printf("ending...\n");
   fclose(ifp);

   return 0;
}



